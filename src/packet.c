/* SPDX-License-Identifier: MIT
 *
 * lwmon packet handling: functions to assemble, send, receive and disassemble
 * packets containing monitoring data
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <lwmon/configure.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#ifdef HAS_ENDIAN_H
#include <endian.h>
#else
#include <sys/endian.h>
#endif
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <lwmon/packet.h>

/* maximum size of hostname sent in a packet (truncated if necessary);
 * note that the length fits in a byte */
#define LWMON_HOSTNAME 255

/* number of packets we store to detect duplicates */
#define NUM_DUPLICATES 64

/* portion of packet we use when checking for duplicates; this includes
 * at least header, timestamp and start of hostname */
#define DUPLICATE_SIZE 64

/* structure of packet header */
typedef struct {
    uint16_t checksum;    /* 16 bit checksum of packet, must be first */
    uint16_t length;      /* total length of paceket */
    uint16_t data;        /* offset to first data item */
    uint8_t  host;        /* offset to host name */
    uint8_t  hostlen;     /* length of host name */
    uint8_t  packetid;    /* packet ID */
    uint8_t  seqno;       /* sequence within a block of packets */
    /* timestamp follows as unsigned integer, and if present, millisecond
     * follows timestamp as unsigned integer */
    /* host name follows immediately after timestamp/millisecond */
} __attribute__((packed)) header_t;

/* structure of data item header */
typedef struct {
    uint16_t length;      /* total length of data item */
    uint16_t type;        /* data type, or 0 if unknown */
    uint8_t  namelen;     /* length of item name */
    uint8_t  parmlen;     /* length of parameter name */
    uint8_t  n_values;    /* number of values */
    /* name follows */
    /* parm follows */
    /* values follows */
} __attribute__((packed)) item_t;

/* buffer to store duplicate detection information */
typedef struct {
    char data[DUPLICATE_SIZE];
    int length;
    int count;
} duplicate_t;

static duplicate_t duplicates[NUM_DUPLICATES];
static int duplicates_count = 0, duplicates_next = 0;

/* buffer to store host name either provided by the caller or determined
 * using gethostname() */
static char host_name_buffer[LWMON_HOSTNAME + 1];
static int host_name_len = 0;

/* packetid helps determining if a packet is duplicate; if the timestamp,
 * packetid and sequence number all match a previously received packet, it
 * is likely to be a duplicate and can be discarded (this library stores
 * the full header, containing packetid and sequence number, as well as
 * timestamp, packet size and number of data items, and the start of the
 * packet data: this reduces the already minimal risk of dropping packets
 * which were not duplicated) */
static int packetid = 0;

/* list of known types; always add more at the end to keep compatibility
 * across versions; also make sure that .number == index + 1 */
const lwmon_datatype_t lwmon_datatypes[] = {
    { 1, "disk", "mountpoint", 9, 9, {
	{ "block_size", NULL, 0 },
	{ "total_blocks", NULL, 0 },
	{ "used_blocks", NULL, 0 },
	{ "free_blocks", NULL, 0 },
	{ "available_blocks", NULL, 0 },
	{ "total_inodes", NULL, 0 },
	{ "used_inodes", NULL, 0 },
	{ "free_inodes", NULL, 0 },
	{ "available_inodes", NULL, 0 },
      }},
    { 2, "file", "pattern", 0, 1, {
	{ "some_found", "none_found", 0 },
      }},
    { 3, "listen", NULL, 1, -1, {
	{ "count", NULL, 0 },
      }},
    { 4, "load", NULL, 3, 4, {
	{ "1minute", NULL, 2 },
	{ "5minute", NULL, 2 },
	{ "15minute", NULL, 2 },
	{ "processors", NULL, 0 },
      }},
    { 5, "memory", NULL, 7, 7, {
	{ "page_size", NULL, 0 },
	{ "total_memory_pages", NULL, 0 },
	{ "used_memory_pages", NULL, 0 },
	{ "free_memory_pages", NULL, 0 },
	{ "total_swap_pages", NULL, 0 },
	{ "used_swap_pages", NULL, 0 },
	{ "free_swap_pages", NULL, 0 },
      }},
    { 6, "network", "interface", 3, 3, {
	{ "interval", NULL, 3 },
	{ "bytes_sent", NULL, 0 },
	{ "bytes_received", NULL, 0 },
      }},
    { 7, "processes", NULL, 1, -1, {
	{ "count", NULL, 0 },
      }},
    { 8, "program", NULL, 4, 4, {
	{ "wallclock", NULL, 3 },
	{ "user", NULL, 3 },
	{ "system", NULL, 3 },
	{ "status", NULL, 0 },
      }},
    { 9, "self", NULL, 3, 4, {
	{ "user", NULL, 3 },
	{ "system", NULL, 3 },
	{ "maxrss", NULL, 0 },
	{ "faults", NULL, 3 },
      }},
    { 10, "usercount", NULL, 1, 1, {
	{ "count", NULL, 0 },
      }},
    { 11, "userlist", "username", 1, 1, {
	{ "count", NULL, 0 },
      }},
    { 12, "cpu", NULL, 3, 3, {
	{ "user", NULL, 3 },
	{ "system", NULL, 3 },
	{ "interval", NULL, 3 },
      }},
    { 0, NULL, NULL, 0, 0, {{ NULL, }, }}
};
static const int n_types = sizeof(lwmon_datatypes) / sizeof(lwmon_datatypes[0]) - 1;

const lwmon_datatype_t * lwmon_datatype(const char * name) {
    int i;
    for (i = 0; lwmon_datatypes[i].name; i++)
	if (strcmp(name, lwmon_datatypes[i].name) == 0)
	    return &lwmon_datatypes[i];
fprintf(stderr, "Undefined type %s\n", name);
    return NULL;
}

/* detect duplicate packets */
static int is_duplicate(const lwmon_packet_t * P) {
    int i, clen = P->length < DUPLICATE_SIZE ? P->length : DUPLICATE_SIZE;
    for (i = 0; i < duplicates_count; i++) {
	if (duplicates[i].length != P->length) continue;
	if (duplicates[i].count != P->data_count) continue;
	if (memcmp(duplicates[i].data, P->data, clen) != 0) continue;
	return 1;
    }
    if (duplicates_count < NUM_DUPLICATES) {
	i = duplicates_count++;
    } else {
	i = duplicates_next++;
	if (duplicates_next >= NUM_DUPLICATES)
	    duplicates_next = 0;
    }
    duplicates[i].length = P->length;
    duplicates[i].count = P->data_count;
    memcpy(duplicates[i].data, P->data, clen);
    return 0;
}

/* set default host name for packets - if this is not called, it will
 * default to the value returned by gethostname(); the data is copied
 * to a new buffer */
void lwmon_packet_hostname(const char * hn) {
    strncpy(host_name_buffer, hn, LWMON_HOSTNAME);
    host_name_buffer[LWMON_HOSTNAME] = 0;
    host_name_len = strlen(host_name_buffer);
}

/* get local host name if another value has not been set */
static int get_host_name(void) {
    if (host_name_len) return host_name_len;
    /* workaround for glibc < 2.2 braindeadness */
    host_name_buffer[0] = 0;
    if (gethostname(host_name_buffer, LWMON_HOSTNAME) < 0) {
	/* workaround for glibc >= 2.2 braindeadness */
	if (errno != ENAMETOOLONG) return 0;
    }
    host_name_buffer[LWMON_HOSTNAME] = 0;
    host_name_len = strlen(host_name_buffer);
    return host_name_len;
}

/* store an integer using the same format as the "w" template in perl pack */
static inline void add_uint(unsigned char data[], int * len, uint64_t val) {
    uint64_t tmpval;
    int ndigits, base;
    tmpval = val >> 7;
    ndigits = 1;
    while (tmpval > 0) {
	tmpval >>= 7;
	ndigits++;
    }
    base = *len;
    *len = base + ndigits;
    ndigits--;
    data[base + ndigits] = val & 0x7f;
    while (ndigits > 0) {
	ndigits--;
	val >>= 7;
	data[base + ndigits] = (val & 0x7f) | 0x80;
    }
}

/* same as add_uint, but encode the sign by storing 2*abs(num)+sign(num) */
static inline void add_int(unsigned char data[], int * len, int64_t val) {
    uint64_t absval;
    int sign;
    /* do it this way to avoid overflow */
    if (val < 0) {
	absval = -val;
	sign = 1;
    } else {
	absval = val;
	sign = 0;
    }
    absval <<= 1;
    absval |= sign;
    add_uint(data, len, absval);
}

/* unpacks integer packed by add_uint() */
static inline int get_uint(const unsigned char * data, int len, uint64_t *val) {
    uint64_t res = 0;
    int go = 1, dl = 0;
    while (go && dl < len) {
	res <<= 7;
	res |= data[dl] & 0x7f;
	if (! (data[dl] & 0x80)) go = 0;
	dl++;
    }
    *val = res;
    if (go) return -1;
    return dl;
}

/* unpacks integer packed by add_int() */
static inline int get_int(const unsigned char * data, int len, int64_t * val) {
    uint64_t absval;
    int sign, dl = get_uint(data, len, &absval);
    if (dl < 0) return dl;
    sign = absval & 1;
    absval >>= 1;
    *val = absval;
    if (sign) *val = -*val;
    return dl;
}

/* quick checksum, similar to the SYSV "sum" program */
static inline uint16_t checksum(const lwmon_packet_t * P) {
    int i;
    uint32_t chk;
    for (i = 2, chk = 0; i < P->length; i++)
	chk += P->data[i];
    chk = (chk & 0xffff) + (chk >> 16);
    return (chk & 0xffff) + (chk >> 16);
}

/* like initpacket, but leaves some fields unchanged to indicate that
 * the packets are part of the same transmission */
void lwmon_reinitpacket(lwmon_packet_t * P) {
    header_t * header = (void *)P->data;
    /* checksum and length will be filled in when closing the packet */
    header->checksum = htobe16(0);
    header->length = htobe16(0);
    header->seqno++;
    P->data_count = 0;
    P->command = 0;
    P->cmdptr = 0;
    P->length = P->header_len;
}

/* initialise packet and fill host name in */
void lwmon_initpacket(lwmon_packet_t * P) {
    struct timespec now;
    header_t * header = (void *)P->data;
    int len = sizeof(*header);
    get_host_name();
    packetid++;
    if (packetid <= 0 || packetid > 0xff) packetid = 1;
    header->hostlen = host_name_len;
    header->packetid = packetid;
    /* timestamp follows just after the packet header */
    clock_gettime(CLOCK_REALTIME, &now);
    add_uint(P->data, &len, now.tv_sec);
    add_uint(P->data, &len, now.tv_nsec / 1000000);
    /* and hostname follows timestamp */
    header->host = len;
    if (host_name_len)
	memcpy(&P->data[len], host_name_buffer, host_name_len);
    len += host_name_len;
    P->header_len = len;
    header->data = htobe16(len);
    header->seqno = 0;
    lwmon_reinitpacket(P);
}

/* common code between lwmon_receivepacket and lwmon_readpacket: decode
 * packet in buffer */
#ifdef EBADMSG
#define BAD_PACKET EBADMSG
#else
#define BAD_PACKET EINVAL
#endif
static inline int decodepacket(lwmon_packet_t * P, size_t datasize) {
    header_t * header = (void *)P->data;
    size_t len, ptr;
    if (datasize < sizeof(*header)) goto is_bad;
    len = be16toh(header->length);
    if (len < sizeof(*header)) goto is_bad;
    if (datasize < len) goto is_bad;
    P->length = len;
    if (be16toh(header->checksum) != checksum(P)) goto is_bad;
    /* scan data items and fill P->itemptr, P->data_count, P->length */
    ptr = be16toh(header->data);
    if (ptr < sizeof(*header)) goto is_bad;
    P->header_len = ptr;
    P->data_count = 0;
    while (ptr < P->length) {
	item_t * item = (void *)&P->data[ptr];
	int rl = P->length - ptr, il;
	if (rl < sizeof(*item)) goto is_bad;
	il = be16toh(item->length);
	if (rl < il) goto is_bad;
	if (be16toh(item->type) == 65535) {
	    /* command */
	    P->command |= (item->namelen << 8) | item->parmlen;
	    P->cmdptr = ptr;
	} else {
	    P->itemptr[P->data_count] = ptr;
	    P->data_count++;
	}
	ptr += il;
    }
    /* check if packet is duplicate */
    if (is_duplicate(P)) {
	errno = ENOMSG;
	return -2;
    }
    return 1;
is_bad:
    errno = BAD_PACKET;
    return -1;
}
#undef BAD_PACKET

/* receives packet from a socket - the packet does not need to be initialised
 * and any data already present will be discarded; return 1 for OK, 0 for end
 * of file/stream, and -1 for error (setting errno); as a special case, if
 * a duplicate packet is received, it returns -2 with errno set to ENOMSG;
 * the last 2 arguments are the same as the last 2 arguments to recvfrom */
int lwmon_receivepacket(int fd, lwmon_packet_t * P,
			struct sockaddr * addr, socklen_t * addrlen)
{
    ssize_t datasize = recvfrom(fd, P->data, sizeof(P->data), 0, addr, addrlen);
    if (datasize < 0) return -1;
    if (datasize == 0) return 0;
    return decodepacket(P, datasize);
}

/* reads packet from file, the packet does not need to be initialised and
 * any data already present will be discarded; return 1 for OK, 0 for end
 * of file and -1 for error (setting errno) */
int lwmon_readpacket(int fd, lwmon_packet_t * P) {
    int ok = -2;
    while (ok == -2) {
	header_t * header = (void *)P->data;
	ssize_t datasize = read(fd, P->data, sizeof(*header));
	if (datasize < 0) return -1;
	if (datasize == 0) return 0;
	if (datasize >= sizeof(*header)) {
	    size_t len = be16toh(header->length);
	    if (len > sizeof(*header)) {
		len -= datasize;
		while (len > 0) {
		    ssize_t nr = read(fd, &P->data[datasize], len);
		    if (nr <= 0) break;
		    datasize += nr;
		    len -= nr;
		}
	    }
	}
	ok = decodepacket(P, datasize);
    }
    return ok;
}

/* add data to a packet, if there is space, return 1 if OK, 0 if no space,
 * -1 if data can never fit in packet */
int lwmon_add_data(lwmon_packet_t * P, const lwmon_dataitem_t * D) {
    unsigned char databuffer[LWMON_ITEMS][32], *ptr;
    int datalen[LWMON_ITEMS];
    item_t * item;
    int space, nl = D->namelen, pl = D->parmlen, di = D->n_values, i;
    if (D->type) {
	if (di < D->type->min_values) return -1;
	if (D->type->max_values >= 0 && di > D->type->max_values) return -1;
    }
    /* do we have space for another item? */
    if (P->data_count >= LWMON_ITEMS) return 0;
    if (nl > LWMON_NAMELEN) return -1;
    if (pl > LWMON_NAMELEN) return -1;
    if (di > LWMON_VALUES) return -1;
    /* calculate space required and prepack data */
    space = sizeof(*item) + nl + pl;
    for (i = 0; i < di; i++) {
	datalen[i] = 0;
	add_int(databuffer[i], &datalen[i], D->values[i]);
	space += datalen[i];
    }
    /* will it fit? */
    if (space + P->header_len > LWMON_PACKET) return -1;
    if (space + P->length > LWMON_PACKET) return 0;
    /* OK, add it */
    P->itemptr[P->data_count] = P->length;
    ptr = &P->data[P->length];
    item = (void *)ptr;
    item->length = htobe16(space);
    item->type = htobe16(D->type ? D->type->number : 0);
    item->namelen = nl;
    item->parmlen = pl;
    item->n_values = di;
    ptr += sizeof(*item);
    if (nl > 0) {
	memcpy(ptr, D->name, nl);
	ptr += nl;
    }
    if (pl > 0) {
	memcpy(ptr, D->parm, pl);
	ptr += pl;
    }
    for (i = 0; i < di; i++) {
	if (datalen[i] > 0) {
	    memcpy(ptr, databuffer[i], datalen[i]);
	    ptr += datalen[i];
	}
    }
    P->length += space;
    P->data_count++;
    return 1;
}

/* add commands to a packet, if there is space: return 1 if OK, 0 if
 * no space; a command can always fit in a new packet, so unlike
 * lwmon_add_data() this function never returns -1 */
int lwmon_add_command(lwmon_packet_t * P, lwmon_command_t command) {
    item_t * item;
    if (! P->cmdptr) {
	/* do we have space to add a command item? */
	if (P->length + sizeof(item_t) > LWMON_PACKET) return 0;
	P->cmdptr = P->length;
	P->length += sizeof(item_t);
	item = (void *)&P->data[P->cmdptr];
	item->length = htobe16(sizeof(*item));
	item->type = htobe16(65535);
	item->namelen = 0;
	item->parmlen = 0;
	item->n_values = 0;
    }
    P->command |= command;
    item = (void *)&P->data[P->cmdptr];
    item->namelen |= command >> 8;
    item->parmlen |= command & 0xff;
    return 1;
}

/* add a log entry to a packet, if there is space: returns 1 if OK, 0 if
 * no space, and -1 if there can never be space in the packet because
 * the text won't fit; the third argument is the log length, which can
 * be specified as -1 to use strlen or the second argument */
int lwmon_add_log(lwmon_packet_t * P, const char * text1, const char * text2) {
    item_t * item;
    unsigned char * ptr;
    int len1 = text1 ? strlen(text1) : 0, len2 = text2 ? strlen(text2) : 0;
    int space = len1 + len2 + sizeof(*item);
    /* do we have space for another item? */
    if (P->data_count >= LWMON_ITEMS) return 0;
    if (len1 > LWMON_NAMELEN || len2 > LWMON_NAMELEN) return -1;
    if (space + P->header_len > LWMON_PACKET) return -1;
    if (space + P->length > LWMON_PACKET) return 0;
    /* OK, add it */
    P->itemptr[P->data_count] = P->length;
    ptr = &P->data[P->length];
    item = (void *)ptr;
    item->length = htobe16(space);
    item->type = htobe16(65534);
    item->namelen = len1;
    item->parmlen = len2;
    item->n_values = 0;
    ptr += sizeof(*item);
    if (text1) {
	memcpy(ptr, text1, len1);
	ptr += len1;
    }
    if (text2) {
	memcpy(ptr, text2, len2);
	ptr += len2;
    }
    P->length += space;
    P->data_count++;
    return 1;
}

static const lwmon_datatype_t * find_type(int number) {
    int n;
    if (number < 1 || number >= 65534) return NULL;
    /* in theory, if things have been kept correctly, the next line will
     * find the type */
    if (number <= n_types && lwmon_datatypes[number - 1].number == number)
	return &lwmon_datatypes[number - 1];
    /* old, slower code, will find the type even if the table has not been
     * kept correctly: this code is going to be replaced with an assert
     * at some point, to indicate that something was wrong with the table */
    for (n = 0; lwmon_datatypes[n].name; n++)
	if (lwmon_datatypes[n].number == number)
	    return &lwmon_datatypes[n];
    return NULL;
}

/* get a data item from the packet; returns 1 if OK, 2 if the item is a log
 * item, 0 if the item is not present, -1 if there is an error decoding the
 * data (setting errno) */
int lwmon_get_data(const lwmon_packet_t * P, int n, lwmon_dataitem_t * D) {
    const unsigned char * ptr;
    const item_t * item;
    int i, len, type, ok = 1;
    if (n < 0 || n >= P->data_count) return 0;
    if (n + 1 == P->data_count)
	len = P->length - P->itemptr[n];
    else
	len = P->itemptr[n + 1] - P->itemptr[n];
    if (len < sizeof(*item)) goto invalid;
    ptr = &P->data[P->itemptr[n]];
    item = (const void *)ptr;
    ptr += sizeof(*item);
    len -= sizeof(*item);
    D->name = (const char *)ptr;
    D->namelen = item->namelen;
    if (len < D->namelen) goto invalid;
    ptr += D->namelen;
    len -= D->namelen;
    D->parm = (const char *)ptr;
    D->parmlen = item->parmlen;
    if (len < D->parmlen) goto invalid;
    ptr += D->parmlen;
    len -= D->parmlen;
    D->n_values = item->n_values;
    type = be16toh(item->type);
    if (type == 65534) ok = 2;
    D->type = find_type(type);
    for (i = 0; i < D->n_values; i++) {
	int dl = get_int(ptr, len, &D->values[i]);
	if (dl < 0) goto invalid;
	ptr += dl;
	len -= dl;
    }
    return ok;
invalid:
    errno = EINVAL;
    return -1;
}

/* close packet by filling in checksum and length */
void lwmon_closepacket(lwmon_packet_t * P) {
    header_t * header = (void *)P->data;
    header->length = htobe16(P->length);
    header->checksum = htobe16(checksum(P));
}

/* produces a human-readable representation of packet */
void lwmon_printpacket(FILE * F, const lwmon_packet_t * P,
		       lwmon_timeformat_t tf, int verbose)
{
    lwmon_packetid_t I;
    char timebuff[32] = "?";
    int n = 0;
    lwmon_packetid(P, &I);
    switch (tf) {
	case lwmon_time_gmt :
	    n = strftime(timebuff, sizeof(timebuff),
		     "%Y-%m-%d %H:%M:%S", gmtime(&I.timestamp));
	    break;
	case lwmon_time_local :
	    n = strftime(timebuff, sizeof(timebuff),
		     "%Y-%m-%d %H:%M:%S", localtime(&I.timestamp));
	    break;
	case lwmon_time_raw :
	    n = snprintf(timebuff, sizeof(timebuff), "%lld", (long long)I.timestamp);
	    break;
    }
    snprintf(&timebuff[n], sizeof(timebuff) - n, ".%03d", I.millisecond);
    fprintf(F, "%s Packet size %d from %.*s\n",
	    timebuff, P->length, I.hostlen, I.host);
    for (n = 0; n < P->data_count; n++) {
	lwmon_dataitem_t D;
	const char * sep = " [", * end = "";
	int ok = lwmon_get_data(P, n, &D), p;
	if (ok == 0) continue;
	if (ok > 0) {
	    fprintf(F, "%s %.*s", timebuff, D.namelen, D.name);
	    if (D.type)
		fprintf(F, " (%s)", D.type->name);
	    else if (ok == 2)
		fprintf(F, " (log entry)");
	    if (D.parmlen > 0)
		fprintf(F, " %.*s", D.parmlen, D.parm);
	    for (p = 0; p < D.n_values; p++) {
		if (verbose && D.type) {
		    int vp = D.type->max_values < 0
			   ? -1 - D.type->max_values
			   : D.type->max_values;
		    if (p < vp) vp = p;
		    if (D.type->values[vp].zero && ! D.values[p]) {
			fprintf(F, "%s%s",
				sep, D.type->values[vp].zero);
		    } else if (D.type->values[vp].zero && D.values[p] == 1) {
			fprintf(F, "%s%s",
				sep, D.type->values[vp].name);
		    } else if (D.type->values[vp].scale > 0) {
			double val = D.values[p];
			int scale = D.type->values[vp].scale;
			while (scale-- > 0) val /= 10.0;
			fprintf(F, "%s%s=%.*f",
				sep, D.type->values[vp].name,
				D.type->values[vp].scale, val);
		    } else {
			fprintf(F, "%s%s=%lld",
				sep, D.type->values[vp].name,
				(long long)D.values[p]);
		    }
		} else {
		    fprintf(F, "%s%lld", sep, (long long)D.values[p]);
		}
		sep = ", ";
		end = "]";
	    }
	} else {
	    fprintf(F, "(Error decoding data item #%d)", n);
	}
	fprintf(F, "%s\n", end);
    }
    fprintf(F, "\n");
    fflush(F);
}

/* calls a function for each data item in a packet */
int lwmon_packetdata(const lwmon_packet_t * P, void * arg,
		     int (*f)(const lwmon_packetid_t *,
			      const lwmon_dataitem_t *, int, void *))
{
    lwmon_packetid_t I;
    int n;
    lwmon_packetid(P, &I);
    for (n = 0; n < P->data_count; n++) {
	lwmon_dataitem_t D;
	int ok = lwmon_get_data(P, n, &D);
	if (ok == 0) continue;
	if (ok < 0) return -1;
	ok = f(&I, &D, ok == 2, arg);
	if (ok < 0) return ok;
    }
    return 0;
}

/* send packet on a socket, in packed binary form; caller must first
 * call lwmon_closepacket(); return 0 if OK, -1 if error (setting errno) */
int lwmon_sendpacket(int fd, const lwmon_packet_t * P) {
    const unsigned char * ptr = P->data;
    ssize_t tw = P->length;
    while (tw > 0) {
	ssize_t nw = write(fd, ptr, tw);
	if (nw <= 0) return -1;
	tw -= nw;
	ptr += nw;
    }
    return 0;
}

/* get packet identification */
void lwmon_packetid(const lwmon_packet_t * P, lwmon_packetid_t * I) {
    uint64_t ts;
    header_t * header = (void *)P->data;
    int tl = get_uint(&P->data[sizeof(*header)], P->length - sizeof(*header), &ts);
    I->timestamp = ts;
    if (tl + sizeof(*header) < header->host) {
	get_uint(&P->data[sizeof(*header) + tl], P->length - sizeof(*header) - tl, &ts);
	I->millisecond = ts;
    } else {
	I->millisecond = 0;
    }
    I->host = (char *)&P->data[header->host];
    I->hostlen = header->hostlen;
    I->id = header->packetid;
    I->seq = header->seqno;
    I->checksum = be16toh(header->checksum);
}

