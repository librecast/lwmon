/* SPDX-License-Identifier: MIT
 *
 * system-dependent functions to retrieve monitoring data -- Linux
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <mntent.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <lwmon/os.h>
#include <lwmon/options.h>

/* size of command name when reading process lists */
#define CMDBUFF 64

/* determine system load and (if known) number of processors */
int lwmon_os_load(lwmon_os_load_t * L) {
    char buffer[128];
    ssize_t nr;
    int fd = open("/proc/loadavg", O_RDONLY);
    if (fd < 0) return -1;
    nr = read(fd, buffer, sizeof(buffer) - 1);
    if (nr < 0) {
	int sve = errno;
	close(fd);
	errno = sve;
	return -1;
    }
    close(fd);
    if (nr < 1) {
	errno = ENFILE;
	return -1;
    }
    buffer[nr] = 0;
    if (sscanf(buffer, "%f%f%f", &L->loads[0], &L->loads[1], &L->loads[2]) < 3)
    {
	errno = EINVAL;
	return -1;
    }
    L->processors = 0;
    fd = open("/sys/devices/system/cpu/online", O_RDONLY);
    if (fd >= 0) {
	nr = read(fd, buffer, sizeof(buffer));
	if (nr > 0) {
	    int ptr, num = 0, onum = -1;
	    for (ptr = 0; ptr < nr; ptr++) {
		if (isdigit(buffer[ptr])) {
		    num = num * 10 + buffer[ptr] - '0';
		} else if (buffer[ptr] == '-') {
		    onum = num;
		    num = 0;
		} else if (buffer[ptr] == ',') {
		    if (onum < 0)
			L->processors++;
		    else
			L->processors += num + 1 - onum;
		    num = 0;
		    onum = -1;
		}
	    }
	    if (onum < 0)
		L->processors++;
	    else
		L->processors += num + 1 - onum;
	}
	close(fd);
    }
    return 0;
}

/* determine memory/swap used, available, total */
int lwmon_os_memory(lwmon_os_memory_t * M) {
    char line[128];
    FILE * F = fopen("/proc/meminfo", "r");
    long kb;
    if (! F) return -1;
    M->page_size = sysconf(_SC_PAGESIZE);
    if (M->page_size == -1) {
	int sve = errno;
	fclose(F);
	errno = sve;
	return -1;
    }
    kb = M->page_size / 1024;
    M->mem_total = -1;
    M->mem_used = -1;
    M->mem_free = -1;
    M->swap_total = -1;
    M->swap_used = -1;
    M->swap_free = -1;
    while (fgets(line, sizeof(line), F)) {
#define assign_val(kw, var) \
if (strncmp(line, kw, strlen(kw)) == 0 && line[strlen(kw)] == ':') { \
M->var = atoll(line + strlen(kw) + 1) / kb; \
continue; \
}
	assign_val("MemTotal", mem_total);
	assign_val("MemFree", mem_free);
	assign_val("SwapTotal", swap_total);
	assign_val("SwapFree", swap_free);
#undef assign_val
    }
    if (M->mem_total > 0 && M->mem_free >= 0)
	M->mem_used = M->mem_total - M->mem_free;
    if (M->swap_total > 0 && M->swap_free >= 0)
	M->swap_used = M->swap_total - M->swap_free;
    fclose(F);
    return 0;
}

/* determine CPU time used since some time in the past, in milliseconds */
int lwmon_os_cpu(lwmon_os_cpu_t * C) {
    char line[128];
    FILE * F = fopen("/proc/stat", "r");
    if (! F) return -1;
    while (fgets(line, sizeof(line), F)) {
	long long ut, nt, st;
	if (strncmp(line, "cpu ", 4) != 0) continue;
	if (sscanf(line + 4, "%lld%lld%lld", &ut, &nt, &st) < 3) continue;
	C->user = ut + nt;
	C->system = st;
	fclose(F);
	return 0;
    }
    fclose(F);
    errno = ENOENT;
    return -1;
}

/* call f(pid, processname, arg) for each running process */
int lwmon_os_process_list(int (*f)(pid_t, const char *, void *), void * arg) {
    char procname[32], cmdbuff[CMDBUFF];
    DIR * D = opendir("/proc");
    struct dirent * E;
    int i, ok = 0;
    if (! D) return -1;
    while (ok >= 0 && (E = readdir(D)) != NULL) {
	ssize_t nr;
	pid_t pid;
	int go = 1, fd;
	for (i = 0; E->d_name[i] && ok; i++)
	    if (! isdigit(E->d_name[i]))
		go = 0;
	if (! go) continue;
	pid = atoi(E->d_name);
	sprintf(procname, "/proc/%d/comm", (int)pid);
	fd = open(procname, O_RDONLY);
	if (fd < 0) continue;
	nr = read(fd, cmdbuff, CMDBUFF - 1);
	close(fd);
	if (nr < 0) continue;
	if (nr > 0 && cmdbuff[nr - 1] == '\n') nr--;
	cmdbuff[nr] = 0;
	ok = f(pid, cmdbuff, arg);
    }
    closedir(D);
    return ok;
}

/* call f(fstype, device_path, mount_point, arg) for each mount */
int lwmon_os_disk_list(int (*f)(const char *, const char *,
				const char *, void *),
		       void * arg)
{
    struct mntent * mnt;
    FILE * mounts = setmntent("/etc/mtab", "r");
    int ok = 0;
    if (! mounts) return -1;
    while (ok >= 0 && (mnt = getmntent(mounts)) != NULL) {
	if (strcmp(mnt->mnt_fsname, "rootfs") == 0) continue;
	ok = f(mnt->mnt_type, mnt->mnt_fsname, mnt->mnt_dir, arg);
    }
    endmntent(mounts);
    return ok;
}

/* call f(proto, port, arg) for all listening sockets matching the first
 * argument */
int lwmon_os_listen_list(lwmon_protocol_t proto,
			 int (*f)(lwmon_protocol_t, int, void *),
			 void * arg)
{
    static const struct {
	lwmon_protocol_t proto;
	int listen;
	const char * name;
    } files[] = {
	{ LWMON_PROTO_TCP4, TCP_LISTEN, "/proc/net/tcp" },
	{ LWMON_PROTO_TCP6, TCP_LISTEN, "/proc/net/tcp6" },
	{ LWMON_PROTO_UDP4, 7,          "/proc/net/udp" },
	{ LWMON_PROTO_UDP6, 7,          "/proc/net/udp6" },
	{ LWMON_PROTO_TCP,  0,          NULL }
    };
    int i, ok = 0;
    for (i = 0; ok >= 0 && files[i].name; i++) {
	char line[256];
	FILE * F;
	int first = 1;
	if (! (files[i].proto & proto)) continue;
	F = fopen(files[i].name, "r");
	if (! F) continue;
	while (ok >= 0 && fgets(line, sizeof(line), F)) {
	    const char * lp;
	    int port, st;
	    /* skip header */
	    if (first) {
		first = 0;
		continue;
	    }
	    /* skip "sl" */
	    for (lp = line; *lp && ! isspace((int)(unsigned char)*lp); lp++) ;
	    for (; *lp && *lp != ':'; lp++) ;
	    if (! *lp) continue;
	    lp++;
	    /* skip local address and find port */
	    for (; *lp && *lp != ':'; lp++) ;
	    if (! *lp) continue;
	    lp++;
	    port = strtol(lp, NULL, 16);
	    /* skip remote address and find port */
	    for (; *lp && *lp != ':'; lp++) ;
	    if (! *lp) continue;
	    lp++;
	    /* now find "st" */
	    for (; *lp && ! isspace((int)(unsigned char)*lp); lp++) ;
	    for (; *lp && isspace((int)(unsigned char)*lp); lp++) ;
	    if (! *lp) continue;
	    st = strtol(lp, NULL, 16);
	    if (st != files[i].listen) continue;
	    /* found a listen, is that a port we want? */
	    ok = f(files[i].proto, port, arg);
	}
	fclose(F);
    }
    return ok;
}

/* call f(name, bytes_sent, bytes_recv, arg) for all network interface */
int lwmon_os_interface_list(int (*f)(const char *, int64_t, int64_t, void *),
			    void * arg)
{
    char line[512];
    FILE * F = fopen("/proc/net/dev", "r");
    int ok = 0;
    if (! F) return -1;
    while (ok >= 0 && fgets(line, sizeof(line), F)) {
	char * lp, * ifname;
	long long l_in, l_out;
	    int skip;
	for (lp = line; *lp && isspace((int)(unsigned char)*lp); lp++) ;
	if (! *lp) continue;
	ifname = lp;
	lp = strchr(lp, ':');
	if (! lp) continue;
	*lp++ = 0;
	sscanf(lp, "%lld %d %d %d %d %d %d %d %lld",
	       &l_in, &skip, &skip, &skip, &skip, &skip, &skip, &skip, &l_out);
	ok = f(ifname, l_out, l_in, arg);
    }
    fclose(F);
    return ok;
}

