/* SPDX-License-Identifier: MIT
 *
 * functions to parse command-line options and configuration files
 * for lwmon and awooga
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <lwmon/configure.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include <netdb.h>
#ifdef HAS_ENDIAN_H
#include <endian.h>
#else
#include <sys/endian.h>
#endif
#include <dirent.h>
#include <fnmatch.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <lwmon/options.h>

/* maximum input line size when reading configuration files */
#define LINE_SIZE 4096

/* maximum number of fields per line */
#define LINE_FIELDS 64

/* maximum nesting of conditionals */
#define LWMON_MAX_NESTING 16

/* structure to store information about conditionals */
struct lwmon_condstore_s {
    int nest;                             /* current nesting level */
    int effective[LWMON_MAX_NESTING + 1]; /* current effective condition */
    int value[LWMON_MAX_NESTING + 1];     /* value of last if/else/elsif */
    int can[LWMON_MAX_NESTING + 1];       /* can a future else/elsif be true */
    int had_else[LWMON_MAX_NESTING + 1];  /* have we seen an else */
};

static int argc, argp;
static char **argv;
static const char * progname;

/* list of protocol names we understand: udp, tcp, udp4, tcp4, udp6, tcp6
 * this is different from what we get from something like getprotobyname */
static struct {
    const char * name;
    lwmon_protocol_t proto;
} protocols[] = {
    { "udp",    LWMON_PROTO_UDP },
    { "udp4",   LWMON_PROTO_UDP4 },
    { "udp6",   LWMON_PROTO_UDP6 },
    { "tcp",    LWMON_PROTO_TCP },
    { "tcp4",   LWMON_PROTO_TCP4 },
    { "tcp6",   LWMON_PROTO_TCP6 },
    { NULL, }
};

/* lwmon_initops must be called before any other functions, providing
 * the argc/argv argument from main() */
void lwmon_initops(int ac, char *av[]) {
    if (ac > 0) {
	progname = strrchr(av[0], '/');
	if (progname) progname++; else progname = av[0];
	ac--;
	av++;
    } else {
	progname = "?";
    }
    argc = ac;
    argv = av;
    argp = 0;
}

static int parse_int(char opt, const char * arg, int, char *[],
		     lwmon_optdata_t data);
static int parse_string(char opt, const char * arg, int, char *[],
			lwmon_optdata_t data);
static int parse_filename(char opt, const char * arg, int, char *[],
			  lwmon_optdata_t data);
static int parse_ioname(char opt, const char * arg, int, char *[],
			lwmon_optdata_t data);
static int parse_dirname(char opt, const char * arg, int, char *[],
			 lwmon_optdata_t data);

/* parse options and store arguments as appropriate; returns 1 after
 * parsing all options, 0 if there is an error */
int lwmon_opts(lwmon_opts_t *optsp[]) {
    while (argc > 0 && argv[0][0] == '-') {
	char * ap = &argv[0][1];
	argc--;
	argv++;
	if (ap[0] == '-' && ! ap[1]) return 1;
	while (*ap) {
	    char opt = *ap++;
	    int ok = 0, outer;
	    for (outer = 0; optsp[outer] && ! ok; outer++) {
		lwmon_opts_t * opts = optsp[outer];
		int ptr;
		for (ptr = 0; opts[ptr].argtype != LWMON_OPT_END; ptr++) {
		    int (*func)(char, const char *, int, char *[],
				lwmon_optdata_t) = NULL;
		    if (opts[ptr].argtype == LWMON_OPT_TITLE) continue;
		    if (opts[ptr].optname != opt) continue;
		    switch (opts[ptr].argtype) {
			case LWMON_OPT_END : func = NULL; break;
			case LWMON_OPT_TITLE : func = NULL; break;
			case LWMON_OPT_INT : func = parse_int; break;
			case LWMON_OPT_STRING : func = parse_string; break;
			case LWMON_OPT_IO : func = parse_ioname; break;
			case LWMON_OPT_FILE : func = parse_filename; break;
			case LWMON_OPT_DIR : func = parse_dirname; break;
			case LWMON_OPT_FUNC : func = opts[ptr].func; break;
			case LWMON_OPT_NONE : func = NULL; break;
		    }
		    if (func) {
			int skip;
			if (! *ap) {
			    if (argc == 0) {
				fprintf(stderr,
					"%s: Missing argument for -%c\n",
					progname, opt);
				return 0;
			    }
			    ap = argv[0];
			    argc--;
			    argv++;
			}
			skip = func(opt, ap, argc, argv, opts[ptr].data);
			if (skip < 0) return 0;
			argc -= skip;
			argv += skip;
			ap = "";
		    } else {
			*opts[ptr].data.i = 1;
		    }
		    ok = 1;
		    break;
		}
	    }
	    if (! ok) {
		fprintf(stderr, "%s: Unknown option -%c\n", progname, opt);
		return 0;
	    }
	}
    }
    return 1;
}

/* format a line to fit current screen width */
static void fmt(FILE * F, const char * indent, int width, const char * line) {
    int len = strlen(indent), max = width - len;
    char icopy[len + 1];
    if (! line || ! *line) return;
    memset(icopy, ' ', len);
    icopy[len] = 0;
    while (1) {
	int len = strlen(line), space = max, i;
	if (len <= max) {
	    fprintf(F, "%s%s\n", indent, line);
	    return;
	}
	for (i = 0; i < max; i++)
	    if (isspace((int)(unsigned char)line[i]))
		space = i;
	while (space > 0 && isspace((int)(unsigned char)line[space - 1]))
	    space--;
	if (space < 1) space = max;
	fprintf(F, "%s%.*s\n", indent, space, line);
	while (space < len && isspace((int)(unsigned char)line[space]))
	    space++;
	line += space;
	indent = icopy;
    }
}

/* functions to print short and long usage; the first argument is normally
 * stderr or stdout and the second argument is the one-line description
 * of non-option arguments (description for option arguments is derived
 * from the options array) */

void lwmon_shortusage(FILE * F, const char * oneline) {
    fprintf(F, "Usage: %s [options] %s\n", progname, oneline);
    fprintf(F, "To show options run: %s -h\n", progname);
}

void lwmon_usage(FILE * F, const char * oneline, lwmon_opts_t * const optsp[],
		 lwmon_opthelp_t * const helpp[])
{
    struct winsize winsize;
    char indent[25];
    int outer, optlen = 0, width = 0;
    if (ioctl(fileno(stdout), TIOCGWINSZ, &winsize) >= 0)
	width = winsize.ws_col;
    if (width == 0) width = 80;
    fprintf(F, "Usage: %s [options] %s\n", progname, oneline);
    for (outer = 0; optsp[outer]; outer++) {
	const lwmon_opts_t *opts = optsp[outer];
	int ptr;
	for (ptr = 0; opts[ptr].argtype != LWMON_OPT_END; ptr++) {
	    int len;
	    switch (opts[ptr].argtype) {
		case LWMON_OPT_INT :
		case LWMON_OPT_STRING :
		case LWMON_OPT_IO :
		case LWMON_OPT_FILE :
		case LWMON_OPT_DIR :
		case LWMON_OPT_FUNC :
		    len = strlen(opts[ptr].argname);
		    if (optlen < len) optlen = len;
		    break;
		case LWMON_OPT_END :
		case LWMON_OPT_TITLE :
		case LWMON_OPT_NONE :
		    break;
	    }
	}
    }
    if (optlen > width / 10) optlen = width / 10;
    if (optlen < 4) optlen = 4;
    if (optlen > sizeof(indent) - 8) optlen = sizeof(indent) - 8;
    for (outer = 0; optsp[outer]; outer++) {
	const lwmon_opts_t *opts = optsp[outer];
	int ptr;
	for (ptr = 0; opts[ptr].argtype != LWMON_OPT_END; ptr++) {
	    int len;
	    switch (opts[ptr].argtype) {
		case LWMON_OPT_END :
		    break;
		case LWMON_OPT_TITLE :
		    fprintf(F, "\n");
		    fmt(F, "", width, opts[ptr].argname);
		    fprintf(F, "\n");
		    break;
		case LWMON_OPT_INT :
		case LWMON_OPT_STRING :
		case LWMON_OPT_IO :
		case LWMON_OPT_FILE :
		case LWMON_OPT_DIR :
		case LWMON_OPT_FUNC :
		    len = strlen(opts[ptr].argname);
		    if (len > optlen) {
			fprintf(F, "  -%c %s\n",
				opts[ptr].optname, opts[ptr].argname);
		        sprintf(indent, "     %-*s  ", optlen, "");
			fmt(F, indent, width, opts[ptr].helptext);
		    } else {
		        sprintf(indent, "  -%c %-*.*s  ",
				opts[ptr].optname,
				optlen, optlen,  opts[ptr].argname);
			fmt(F, indent, width, opts[ptr].helptext);
		    }
		    break;
		case LWMON_OPT_NONE :
		    sprintf(indent, "  -%c %-*s  ",
			    opts[ptr].optname, optlen,  "");
		    fmt(F, indent, width, opts[ptr].helptext);
		    break;
	    }
	}
    }
    fprintf(F, "\n");
    if (! helpp || ! helpp[0]) return;
    for (outer = 0; helpp[outer]; outer++) {
	lwmon_opthelp_t *help = helpp[outer];
	int ptr;
	for (ptr = 0; help[ptr].mode != LWMON_OH_END; ptr++) {
	    switch (help[ptr].mode) {
		case LWMON_OH_END :
		    break;
		case LWMON_OH_SPACE :
		    fprintf(F, "\n");
		    break;
		case LWMON_OH_PARA :
		    fmt(F, "", width, help[ptr].line);
		    break;
		case LWMON_OH_INDENT :
		    fmt(F, "  ", width, help[ptr].line);
		    break;
	    }
	}
    }
    fprintf(F, "\n");
}

/* after running lwmon_opts there may be non-option arguments;
 * lwmon_argcount returns the number of such arguments;
 * lwmon_firstarg returns the first argument (or NULL if there are none)
 * lwmon_nextarg returns the next argument (NULL if there are no more arguments)
 * lwmon_firstarg must be called before calling lwmon_nextarg the first time */

int lwmon_argcount(void) {
    return argc;
}

char * lwmon_firstarg(void) {
    if (argc < 1) return NULL;
    argp = 1;
    return argv[0];
}

char * lwmon_nextarg(void) {
    if (argc <= argp) return NULL;
    return argv[argp++];
}

/* convert string to number, wrapper around strtol() checking for errors */
static int getint(const char ** arg, int * val) {
    char * ep;
    long result;
    errno = 0;
    result = strtol(*arg, &ep, 10);
    if ((errno == ERANGE && (result == LONG_MIN || result == LONG_MAX)) ||
	(errno != 0 && result == 0))
    {
	perror(*arg);
	return 0;
    }
    if (ep == *arg) {
	fprintf(stderr, "%s: no digits found\n", *arg);
	return 0;
    }
    if (*ep) {
	fprintf(stderr, "%s: extra data after number\n", *arg);
	return 0;
    }
    *arg = ep;
    *val = result;
    return 1;
}

static int parse_int(char opt, const char * arg, int argc, char *argv[],
		     lwmon_optdata_t data)
{
    return getint(&arg, data.i) ? 0 : -1;
}

static int parse_string(char opt, const char * arg, int argc, char *argv[],
			lwmon_optdata_t data)
{
    *data.s = arg;
    return 0;
}

static int parse_ioname(char opt, const char * arg, int argc, char *argv[],
			lwmon_optdata_t data)
{
    if (arg[0] == '-' && ! arg[1]) {
	/* OK: stdin/out */
    } else if (arg[0] == '/') {
	/* OK: absolute path */
    } else {
	fprintf(stderr, "%s: not an absolute path\n", arg);
	return -1;
    }
    *data.s = arg;
    return 0;
}

static int parse_filename(char opt, const char * arg, int argc, char *argv[],
			  lwmon_optdata_t data)
{
    if (arg[0] == '/') {
	/* OK: absolute path */
    } else {
	fprintf(stderr, "%s: not an absolute path\n", arg);
	return -1;
    }
    *data.s = arg;
    return 0;
}

static int parse_dirname(char opt, const char * arg, int argc, char *argv[],
			 lwmon_optdata_t data)
{
    struct stat sbuff;
    if (arg[0] != '/') {
	fprintf(stderr, "%s: not an absolute path\n", arg);
	return -1;
    }
    if (stat(arg, &sbuff) < 0) {
	perror(arg);
	return -1;
    }
    if (! S_ISDIR(sbuff.st_mode)) {
	fprintf(stderr, "%s: not a directory\n", arg);
	return -1;
    }
    *data.s = arg;
    return 0;
}

int lwmon_check_file(const char * kw, int num,
		     const char * arg, lwmon_parsedata_t * res)
{
    if (*arg != '/') {
	fprintf(stderr, "Not an absolute path: %s\n", arg);
	return 0;
    }
    return lwmon_check_string(kw, num, arg, res);
}

int lwmon_check_file_or_stdout(const char * kw, int num,
			       const char * arg, lwmon_parsedata_t * res)
{
    if (*arg != '/' && strcmp(arg, "-") != 0) {
	fprintf(stderr, "Not an absolute path or '-': %s\n", arg);
	return 0;
    }
    return lwmon_check_string(kw, num, arg, res);
}

int lwmon_check_program(const char * kw, int num,
			const char * arg, lwmon_parsedata_t * res)
{
    struct stat sbuff;
    if (*arg != '/') {
	fprintf(stderr, "Not an absolute path: %s\n", arg);
	return 0;
    }
    if (stat(arg, &sbuff) < 0) {
	perror(arg);
	return 0;
    }
    if (! (sbuff.st_mode & S_IXUSR)) {
	fprintf(stderr, "Not executable: %s\n", arg);
	return 0;
    }
    return lwmon_check_string(kw, num, arg, res);
}

int lwmon_check_freq(const char * kw, int num,
		     const char * arg, lwmon_parsedata_t * res)
{
    int freq;
    if (! getint(&arg, &freq)) return 0;
    if (freq < 1) {
	fprintf(stderr, "Invalid frequency %d\n", freq);
	return 0;
    }
    res->iv = freq;
    return 1;
}

int lwmon_check_int(const char * kw, int num,
		    const char * arg, lwmon_parsedata_t * res)
{
    int val;
    if (! getint(&arg, &val)) return 0;
    res->iv = val;
    return 1;
}

int lwmon_check_string(const char * kw, int num,
		       const char * arg, lwmon_parsedata_t * res)
{
    res->iv = strlen(arg);
    res->sp = strdup(arg);
    if (! res->sp) {
	perror(NULL);
	return 0;
    }
    return 1;
}

int lwmon_check_proto(const char * kw, int num,
		      const char * arg, lwmon_parsedata_t * res)
{
    int i;
    for (i = 0; protocols[i].name; i++) {
	if (strcasecmp(arg, protocols[i].name) == 0) {
	    res->iv = protocols[i].proto;
	    return 1;
	}
    }
    fprintf(stderr, "Unknown protocol: %s\n", arg);
    return 0;
}

int lwmon_check_port(lwmon_protocol_t proto, const char * arg,
		     lwmon_parsedata_t * res)
{
    int i, is_num = 1;
    for (i = 0; arg[i]; i++)
	if (! isdigit((int)(unsigned char)arg[i]))
	    is_num = 0;
    if (is_num) {
	res->iv = atoi(arg);
	if (res->iv < 1 || res->iv > 65535) {
	    fprintf(stderr, "Port number out of range: %s\n", arg);
	    return 0;
	}
    } else {
	struct servent * se;
	const char * pname = "";
	switch (proto) {
	    case LWMON_PROTO_UDP :
	    case LWMON_PROTO_UDP4 :
	    case LWMON_PROTO_UDP6 :
		pname = "udp";
		break;
	    case LWMON_PROTO_TCP :
	    case LWMON_PROTO_TCP4 :
	    case LWMON_PROTO_TCP6 :
		pname = "tcp";
		break;
	}
	se = getservbyname(arg, pname);
	if (! se) {
	    fprintf(stderr, "Unknown service: %s\n", arg);
	    return 0;
	}
	res->iv = be16toh(se->s_port);
    }
    return 1;
}

static inline int skip_spaces(const char * line, int p) {
    /* some libc's have isspace as a macro expanding to an array and
     * char as signed - so need the series of casts to make thing work;
     * no idea why they don't do that in the macro... */
    while (line[p] && isspace((int)(unsigned char)line[p]))
	p++;
    return p;
}

static inline int skip_nonspace(const char * line, int p) {
    while (line[p] && ! isspace((int)(unsigned char)line[p]))
	p++;
    return p;
}

static inline char has_quote(char * line, int p) {
    if (line[p] == '"' || line[p] == '\'') return line[p];
    if (line[p] == '[') return ']';
    return 0;
}

static inline const char * skip_field(char * line, int * _p) {
    int p = *_p;
    const char * start = &line[p];
    char eq = has_quote(line, p);
    if (eq) {
	p++;
	start = &line[p];
	while (line[p] && line[p] != eq) p++;
	if (line[p]) line[p++] = 0;
    } else {
	p = skip_nonspace(line, p);
	if (line[p]) {
	    line[p++] = 0;
	    p = skip_spaces(line, p);
	}
    }
    *_p = p;
    return start;
}

static int parse_line(const char * fname, char * line,
		      const lwmon_parse_t * C[], lwmon_condstore_t * S)
{
    const char * kw;
    int o, ptr = strlen(line);
    if (ptr < 1) return 1;
    ptr = skip_spaces(line, 0);
    kw = skip_field(line, &ptr);
    for (o = 0; C[o]; o++) {
	int n;
	for (n = 0; C[o][n].kw; n++) {
	    lwmon_parsedata_t data[LINE_FIELDS];
	    const char * fields[LINE_FIELDS];
	    int n_fields = 0, f, ok = 0;
	    if (strcasecmp(C[o][n].kw, kw) != 0) continue;
	    while (line[ptr] && n_fields < LINE_FIELDS) {
		fields[n_fields] = skip_field(line, &ptr);
		data[n_fields].sp = NULL;
		data[n_fields].iv = 0;
		n_fields++;
	    }
	    if (n_fields < C[o][n].minargs) {
		fprintf(stderr,
			"%sNot enough arguments for %s (min %d)\n",
			fname, kw, C[o][n].minargs);
		return 0;
	    }
	    if (C[o][n].repeat_check == NULL && n_fields > C[o][n].maxargs) {
		fprintf(stderr,
			"%sToo many arguments for %s (max %d)\n",
			fname, kw, C[o][n].maxargs);
		return 0;
	    }
	    for (f = 0; f < C[o][n].maxargs && f < n_fields; f++)
		if (! C[o][n].check[f](kw, f, fields[f], &data[f]))
		    goto fail;
	    for (f = C[o][n].maxargs; f < n_fields; f++)
		if (! C[o][n].repeat_check(kw, f, fields[f], &data[f]))
		    goto fail;
	    if (! C[o][n].store(kw, n_fields, data, S))
		goto fail;
	    /* all went well, so free all data and continue */
	    ok = 1;
	fail:
	    for (f = 0; f < n_fields; f++)
		if (data[f].sp) free(data[f].sp);
	    return ok;
	}
    }
    fprintf(stderr, "%sInvalid keyword \"%s\"\n", fname, kw);
    return 0;
}

int lwmon_parse_line(const char * line, const lwmon_parse_t * C[]) {
    char linecopy[strlen(line) + 1];
    strcpy(linecopy, line);
    return parse_line("", linecopy, C, NULL);
}

int lwmon_parse_file(const char * fname, const lwmon_parse_t * C[]) {
    char line[LINE_SIZE], fcopy[20 + strlen(fname)];
    lwmon_condstore_t S;
    FILE * F = fopen(fname, "rt");
    int lineno = 0;
    if (! F) {
	perror(fname);
	return 0;
    }
    S.nest = 0;
    S.effective[0] = 1;
    while (fgets(line, sizeof(line), F)) {
	int ptr;
	lineno++;
	ptr = strlen(line);
	if (ptr < 1) continue;
	if (line[ptr - 1] == '\n') {
	    line[ptr - 1] = 0;
	    ptr--;
	    if (ptr < 1) continue;
	}
	while (ptr > 0 && line[ptr - 1] == '\\') {
	    ptr--;
	    if (! fgets(line + ptr, sizeof(line) - ptr, F)) {
		fprintf(stderr, "%s: continuation line at end of file\n",
			fname);
		fclose(F);
		return 0;
	    }
	    ptr = strlen(line);
	    if (ptr > 0 && line[ptr - 1] == '\n') {
		line[ptr - 1] = 0;
		ptr--;
	    }
	}
	if (ptr < 1) continue;
	ptr = skip_spaces(line, 0);
	if (! line[ptr] || line[ptr] == '#') continue;
	snprintf(fcopy, sizeof(fcopy), "%s.%d: ", fname, lineno);
	if (parse_line(fcopy, line, C, &S)) continue;
	fclose(F);
	return 0;
    }
    fclose(F);
    if (! S.nest) return 1;
    fprintf(stderr, "%s: end of file inside condition\n", fname);
    return 0;
}

static int is_cfg_file(const struct dirent * name) {
    int l = strlen(name->d_name);
    if (l < 1) return 0;
    if (name->d_name[0] == '.') return 0;
    return 1;
}

/* alphasort does not return a repeatable, consistent, result, depending
 * on the default locale, so we have to write something which does */
static int sort_files(const struct dirent ** a, const struct dirent ** b) {
    return strcmp((*a)->d_name, (*b)->d_name);
}

int lwmon_parse_dir(const char * dname, const lwmon_parse_t * C[]) {
    char filename[strlen(dname) + NAME_MAX + 2];
    struct dirent ** files;
    int nfiles = scandir(dname, &files, is_cfg_file, sort_files), i, ok, fp;
    if (nfiles < 0) {
	perror(dname);
	return 0;
    }
    strcpy(filename, dname);
    fp = strlen(dname);
    filename[fp++] = '/';
    for (i = 0, ok = 1; i < nfiles; i++) {
	if (ok) {
	    if (strlen(files[i]->d_name) > NAME_MAX) {
		fprintf(stderr, "Filename too long in %s: %s\n",
			dname, files[i]->d_name);
		ok = 0;
	    } else {
		strcpy(filename + fp, files[i]->d_name);
		if (! lwmon_parse_file(filename, C))
		    ok = 0;
	    }
	}
	free(files[i]);
    }
    free(files);
    return ok;
}

/* functions to handle a conditional within a configuration file */

int lwmon_cond_find(const lwmon_condition_t condtypes[], const char * arg) {
    int i;
    for (i = 0; condtypes[i].name; i++)
	if (strcmp(arg, condtypes[i].name) == 0)
	    return i;
    fprintf(stderr, "Invalid condition %s\n", arg);
    return -1;
}

int lwmon_if(const lwmon_condstore_t * C) {
    return C == NULL || C->effective[C->nest];
}

int lwmon_cond_if(const lwmon_condition_t * C, const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int value;
    if (! cond) {
	fprintf(stderr, "Conditional not permitted here\n");
	return 0;
    }
    if (cond->nest >= LWMON_MAX_NESTING) {
	fprintf(stderr, "Conditional nested too deep\n");
	return 0;
    }
    value = C->cond(argc, D, C->flags);
    cond->nest++;
    cond->value[cond->nest] = value;
    cond->can[cond->nest] = ! value;
    cond->had_else[cond->nest] = 0;
    cond->effective[cond->nest] =
	cond->value[cond->nest] && cond->effective[cond->nest - 1];
    return 1;
}

int lwmon_cond_elsif(const lwmon_condition_t * C, const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! cond) {
	fprintf(stderr, "Conditional not permitted here\n");
	return 0;
    }
    if (cond->nest < 1) {
	fprintf(stderr, "%s outside conditional\n", kw);
	return 0;
    }
    if (cond->had_else[cond->nest]) {
	fprintf(stderr, "%s after else\n", kw);
	return 0;
    }
    if (cond->can[cond->nest]) {
	int value = C->cond(argc, D, C->flags);
	cond->value[cond->nest] = value;
	cond->can[cond->nest] = ! value;
    } else {
	cond->value[cond->nest] = 0;
    }
    cond->effective[cond->nest] =
	cond->value[cond->nest] && cond->effective[cond->nest - 1];
    return 1;
}

int lwmon_cond_else(const char * kw, int argc,
		    lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! cond) {
	fprintf(stderr, "Conditional not permitted here\n");
	return 0;
    }
    if (cond->nest < 1) {
	fprintf(stderr, "%s outside conditional\n", kw);
	return 0;
    }
    if (cond->had_else[cond->nest]) {
	fprintf(stderr, "Duplicate %s\n", kw);
	return 0;
    }
    cond->value[cond->nest] = cond->can[cond->nest];
    cond->can[cond->nest] = 0;
    cond->had_else[cond->nest] = 1;
    cond->effective[cond->nest] =
	cond->value[cond->nest] && cond->effective[cond->nest - 1];
    return 1;
}

int lwmon_cond_endif(const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! cond) {
	fprintf(stderr, "Conditional not permitted here\n");
	return 0;
    }
    if (cond->nest < 1) {
	fprintf(stderr, "%s outside conditional\n", kw);
	return 0;
    }
    cond->nest--;
    return 1;
}

static int is_glob(const char * pattern) {
    while (*pattern) {
	char c = *pattern++;
	if (c == '*' || c == '?' || c == '[') return 1;
    }
    return 0;
}

static int find_file(const char * dir, int dirlen,
		     const char * pattern, int data)
{
    DIR * D;
    struct dirent * E;
    char namebuff[dirlen + 2 + NAME_MAX];
    int found = 0;
    strncpy(namebuff, dir, dirlen);
    namebuff[dirlen] = 0;
    if (! is_glob(pattern)) {
	struct stat sbuff;
	if (strlen(pattern) > NAME_MAX) return 0;
	namebuff[dirlen++] = '/';
	strcpy(namebuff + dirlen, pattern);
	if (stat(namebuff, &sbuff) < 0) return 0;
	if (data && sbuff.st_size < 1) return 0;
	return 1;
    }
    D = opendir(namebuff);
    if (! D) return 0;
    namebuff[dirlen++] = '/';
    while (! found && (E = readdir(D)) != NULL) {
	if (fnmatch(pattern, E->d_name, 0) != 0) continue;
	if (data) {
	    struct stat sbuff;
	    if (strlen(E->d_name) > NAME_MAX) continue;
	    strcpy(namebuff + dirlen, E->d_name);
	    if (stat(namebuff, &sbuff) < 0) continue;
	    if (sbuff.st_size < 1) continue;
	}
	found = 1;
    }
    closedir(D);
    return found;
}

static int find_dir(const char * dir, int dirlen,
		    const char * pat, int patlen,
		    const char * rest, int data)
{
    DIR * D;
    struct dirent * E;
    const char * slash = strchr(rest, '/');
    char namebuff[dirlen + 2 + NAME_MAX], pattern[1 + patlen];
    int found = 0;
    strncpy(namebuff, dir, dirlen);
    strncpy(pattern, pat, patlen);
    pattern[patlen] = 0;
    if (! is_glob(pattern)) {
	struct stat sbuff;
	if (patlen > NAME_MAX) return 0;
	if (dirlen > 0 && namebuff[dirlen - 1] == '/') dirlen--;
	namebuff[dirlen++] = '/';
	strcpy(namebuff + dirlen, pattern);
	if (stat(namebuff, &sbuff) < 0) return 0;
	if ((sbuff.st_mode & S_IFMT) != S_IFDIR) return 0;
	if (slash)
	    return find_dir(namebuff, dirlen + patlen,
			    rest, slash - rest, slash + 1, data);
	else
	    return find_file(namebuff, dirlen + patlen, rest, data);
    }
    namebuff[dirlen] = 0;
    D = opendir(namebuff);
    if (! D) return 0;
    if (dirlen > 0 && namebuff[dirlen - 1] == '/') dirlen--;
    namebuff[dirlen++] = '/';
    while (! found && (E = readdir(D)) != NULL) {
	struct stat sbuff;
	if (fnmatch(pattern, E->d_name, 0) != 0) continue;
	if (strlen(E->d_name) > NAME_MAX) continue;
	strcpy(namebuff + dirlen, E->d_name);
	if (stat(namebuff, &sbuff) < 0) continue;
	if ((sbuff.st_mode & S_IFMT) != S_IFDIR) continue;
	if (slash)
	    found = find_dir(namebuff, dirlen + strlen(E->d_name),
			     rest, slash - rest, slash + 1, data);
	else
	    found = find_file(namebuff, dirlen + strlen(E->d_name),
			      rest, data);
    }
    closedir(D);
    return found;
}

static int has_file(const char * pattern, int data) {
#if 0
    glob_t G;
    int res = glob(pattern, GLOB_NOSORT, NULL, &G);
    int ok = res == 0 && G.gl_pathc > 0;
    if (ok && data) {
	int p;
	ok = 0;
	for (p = 0; ! ok && p < G.gl_pathc; p++) {
	    struct stat sbuff;
	    if (stat(G.gl_pathv[p], &sbuff) < 0) continue;
	    if (sbuff.st_size > 0) ok = 1;
	}
    }
    globfree(&G);
    return ok;
#else
    const char * slash, * sp = pattern, * root = ".";
    if (sp[0] == '/') {
	root = "/";
	while (sp[0] == '/') sp++;
    }
    if (! sp[0]) return 1;
    slash = strchr(sp, '/');
    if (slash) {
	int len = slash - sp;
	if (find_dir(root, 1, sp, len, slash + 1, data))
	    return 1;
    } else {
	if (find_file(root, 1, sp, data)) return 1;
    }
    return 0;
#endif
}

/* checks if there is a file matching any of the patterns specified in the
 * "sp" field of any of the data; if found, return 1 + the index of the
 * pattern which matched; if none found, return 0;
 * if the third argument is nonzero, empty files are ignored;
 * the two functions differ only in the way the patterns are provided */
int lwmon_has_file(int num, const lwmon_parsedata_t * list, int data) {
    int n;
    for (n = 0; n < num; n++)
	if (has_file(list[n].sp, data))
	    return n + 1;
    return 0;
}

int lwmon_has_file1(int num, const char *patterns[], int data) {
    int n;
    for (n = 0; n < num; n++)
	if (has_file(patterns[n], data))
	    return n + 1;
    return 0;
}

/* checks if the local hostname matches any of the patterns specified in
 * the "sp" field of any of the data, returns 1 if so, 0 if none match */
int lwmon_host_is(int num, const lwmon_parsedata_t * list, int flags) {
    char hostbuff[256];
    int n;
    /* workaround for glibc < 2.2 braindeadness */
    hostbuff[0] = 0;
    if (gethostname(hostbuff, sizeof(hostbuff) - 1) < 0) {
	/* workaround for glibc >= 2.2 braindeadness */
	if (errno != ENAMETOOLONG) return 0;
    }
    hostbuff[sizeof(hostbuff) - 1] = 0;
    for (n = 0; n < num; n++)
	if (fnmatch(list[n].sp, hostbuff, 0) == 0)
	    return 1;
    return 0;
}

