/* SPDX-License-Identifier: MIT
 *
 * light weight monitoring - collects data and save to file(s) or sends it
 * to another copy of lwmon, or to the awooga monitoring system
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <utmpx.h>
#include <utmp.h>
#include <time.h>
#include <poll.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/statvfs.h>
#include <sys/wait.h>
#include <fnmatch.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <lwmon/options.h>
#include <lwmon/packet.h>
#include <lwmon/os.h>

/* maximum number of results of pattern expansion, this limits the
 * number of mount points handled by a single "disk" and the number
 * of network interfaces handled by a single "network" */
#define MAX_RESULTS 256

/* maximum number of socket (unix or UDP) sources */
#define MAX_RECEIVES 32

/* maimum number of socket destinations */
#define MAX_SENDS 32

/* maximum length of list sent by "userlist" */
#define MAX_USERS 32

/* maximum total length of usernames sent by "userlist" */
#define MAX_USERLEN 4096

/* check retry time: if a check fails we retry it after this many
 * seconds rather than its normal frequency */
#define RETRY_TIME 5

/* check results */
typedef enum {
    CHECK_STOP,
    CHECK_RETRY,
    CHECK_CONTINUE
} check_result_t;

/* generic check */
typedef struct check_s check_t;
struct check_s {
    check_t * next;
    /* number of seconds between checks */
    int freq;
    /* time of next check */
    time_t next_check;
    /* time of last check */
    struct timespec last_check;
    /* is this check still running? */
    int is_running;
    /* is this check a program check? */
    int is_program;
    /* name sent with the data */
    const char * name;
    /* length of the "name" element */
    int namelen;
    /* type of data produced by this check, or NULL for a generic
     * "list of integers" */
    const lwmon_datatype_t * type;
    /* function called to run the check; it is given the packet under
     * construction and this structure as arguments */
    check_result_t (*run)(lwmon_packet_t *, const check_t *);
    /* is a re-run command allowed on this, and if so under which
     * conditions? */
    int re_run;
    /* flags and data_count are reserved for the "run" function */
    int flags;
    int data_count;
    /* data and extra point to any extra data allocated with this structure
     * and are reserved for the "run" function */
    void * data;
    void * extra;
};

/* structure used during pattern expansion */
typedef struct {
    /* patterns to expand */
    int n_patterns;
    const lwmon_parsedata_t * patterns;
    /* result of expansion */
    int n_results;
    lwmon_parsedata_t results[MAX_RESULTS];
} pattern_t;

/* structure used while checking "processes" */
typedef struct {
    /* patterns to match against command name */
    int n_patterns;
    const char ** patterns;
    /* array containing the number of processes matching each pattern */
    int64_t * count;
} process_t;

/* structure used while checking "listen" */
typedef struct {
    /* list of ports to check */
    int n_ports;
    int * ports;
    /* array containing the number of listening sockets per port */
    int64_t * counts;
} port_t;

/* structure used while checking "network" */
typedef struct {
    struct {
	int64_t sent;
	int64_t rcvd;
	struct timespec check;
    } prev, this;
} network_t;

typedef struct {
    pid_t pid;
    struct timespec started;
} program_run_t;

typedef struct {
    lwmon_dataitem_t D;
    lwmon_packet_t * P;
    int n_interfaces;
    network_t * interfaces;
    const char ** names;
} network_check_t;

/* structure storing data for "self" checks */
typedef struct {
    struct timespec when;
    struct timeval utime;
    struct timeval stime;
    long majflt;
} self_t;

typedef struct {
    struct timespec when;
    lwmon_os_cpu_t cpu;
} cpu_t;

/* structure describing a single output */
typedef struct print_s print_t;
struct print_s {
    print_t * next;
    FILE * F;
    int format;
    int append;
    char name[0];
};

/* mapping from conditional keywords to conditional function */
static const lwmon_condition_t condtypes[] = {
    { "has_file",      lwmon_has_file,  0 },
    { "file",          lwmon_has_file,  0 },
    { "nonempty_file", lwmon_has_file,  1 },
    { "nonempty",      lwmon_has_file,  1 },
    { "host_is",       lwmon_host_is,   0 },
    { "host",          lwmon_host_is,   0 },
    { NULL, }
};

static int show_help = 0, show_version = 0, n_sends = 0;
static int sends[MAX_SENDS], daemonise = 0, n_receives = 0;
static int tick = 100, cmd_reopen_ok = -1, cmd_terminate_ok = 0;
static const char * pidfile = NULL, * readfile = NULL;
static char * receive_names[MAX_RECEIVES];
static print_t * prints = NULL;
static check_t * checks = NULL;
static struct pollfd receives[MAX_RECEIVES];
static struct timespec last_reopen;
static lwmon_timeformat_t time_format = lwmon_time_local;

static volatile int interrupt = 0, reopen = 0, end_mode = 0;

static void sig_stop(int sig) {
    interrupt = sig;
}

static void sig_hup(int sig) {
    reopen = sig;
}

static void sig_chld(int sig) {
    /* gathering the data needed to report on "program" checks requires
     * at least one non-async-safe function, so we just change all
     * program checks to make them run immediately and the poll()
     * will terminate with error EINTR so we get right to it as
     * soon as we return from this handler (note that we must even
     * avoid calling wait(), see comments in program_run() */
    check_t * rc;
    for (rc = checks; rc; rc = rc->next) {
	if (! rc->is_running) continue;
	if (rc->is_program) rc->next_check = 0;
    }
}

static int parse_opt_a(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_A(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_c(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_d(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_n(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_o(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_p(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_P(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_r(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_s(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_S(char, const char *, int, char *[], lwmon_optdata_t);
static int parse_opt_t(char, const char *, int, char *[], lwmon_optdata_t);

static lwmon_opts_t options[] = {
    {  0,  "Configuration options", LWMON_OPT_TITLE, },
    { 'c', "FILE", LWMON_OPT_FUNC, parse_opt_c, { .v = NULL },
	   "Read configuration file; can be repeated" },
    { 'd', "DIR", LWMON_OPT_FUNC, parse_opt_d, { .v = NULL },
	   "Read directory of configuration files; can be repeated" },
    { 'o', "OPTION", LWMON_OPT_FUNC, parse_opt_o, { .v = NULL },
	   "Specify a single configuration option; can be repeated" },

    {  0,  "Processing options", LWMON_OPT_TITLE, },
    { 'f', NULL, LWMON_OPT_NONE, NULL, { .i = &daemonise },
	   "Fork and daemonise (see also -i)" },
    { 'i', "FILE", LWMON_OPT_FILE, NULL, { .s = &pidfile },
	   "Write process ID to FILE (after forking if -f also specified)" },
    { 'R', "FILE", LWMON_OPT_FILE, NULL, { .s = &readfile },
	   "Read packed data from FILE, send over all specified outputs and exit" },

    {  0,  "Shortcuts to some \"-o\" options", LWMON_OPT_TITLE, },
    { 'a', "FILE", LWMON_OPT_FUNC, parse_opt_a, { .v = NULL },
	   "Print packets to FILE, appending to existing (-o print)" },
    { 'A', "FILE", LWMON_OPT_FUNC, parse_opt_A, { .v = NULL },
	   "Print details to FILE, appending to existing (-o print)" },
    { 'n', "HOSTNAME", LWMON_OPT_FUNC, parse_opt_n, { .v = NULL },
	   "Specify host name sent with data (-o hostname)" },
    { 'p', "FILE", LWMON_OPT_FUNC, parse_opt_p, { .v = NULL },
	   "Print packets to FILE, overwriting contents (-o print)" },
    { 'P', "FILE", LWMON_OPT_FUNC, parse_opt_P, { .v = NULL },
	   "Print details to FILE, overwriting contents (-o print)" },
    { 'r', "[HOST:]PORT|PATH", LWMON_OPT_FUNC, parse_opt_r, { .v = NULL },
	   "Receive packed data via UDP [HOST:]PORT or local socket PATH (-o receive)" },
    { 's', "HOST:PORT|PATH", LWMON_OPT_FUNC, parse_opt_s, { .v = NULL },
	   "Send packed data to UDP HOST:PORT or local socket PATH (-o send)" },
    { 'S', "FILE", LWMON_OPT_FUNC, parse_opt_S, { .v = NULL },
	   "Sends packed data to FILE (stdout not recommended!) (-o print)" },

    {  0,  "Timestamp options", LWMON_OPT_TITLE, },
    { 't', "FORMAT", LWMON_OPT_FUNC, parse_opt_t, { .v = NULL },
	   "Convert timestamps to FORMAT, one of GMT, LOCAL, RAW, default LOCAL" },

    {  0,  "Help options", LWMON_OPT_TITLE, },
    { 'h', NULL, LWMON_OPT_NONE, NULL, { .i = &show_help },
	   "Show this help text" },
    { 'v', NULL, LWMON_OPT_NONE, NULL, { .i = &show_version },
	   "Show program version" },

    { 0, NULL, LWMON_OPT_END, }
};

static lwmon_opts_t *optlist[] = {
    options,
    NULL
};

static lwmon_opthelp_t *helplist[] = {
    NULL
};

static int c_append(const char *, int, const char *, lwmon_parsedata_t *);
static int c_check(const char *, int, const char *, lwmon_parsedata_t *);
static int c_condition(const char *, int, const char *, lwmon_parsedata_t *);
static int c_disktype(const char *, int, const char *, lwmon_parsedata_t *);
static int c_port(const char *, int, const char *, lwmon_parsedata_t *);
static int c_format(const char *, int, const char *, lwmon_parsedata_t *);

static int s_cmd_measure(const char *, int,
			 lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_cmd_reopen(const char *, int,
			lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_cmd_terminate(const char *, int,
			   lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_cpu(const char *, int,
		 lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_disk(const char *, int,
		  lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_elsif(const char *, int,
		   lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_file(const char *, int,
		  lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_hostname(const char *, int,
		      lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_if(const char *, int,
	        lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_listen(const char *, int,
		    lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_load(const char *, int,
		  lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_memory(const char *, int,
		    lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_network(const char *, int,
		     lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_nonempty(const char *, int,
		      lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_print(const char *, int,
		   lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_program(const char *, int,
		     lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_process(const char *, int,
		     lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_receive(const char *, int,
		     lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_self(const char *, int,
		  lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_send(const char *, int,
		  lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_usercount(const char *, int,
		      lwmon_parsedata_t *, lwmon_condstore_t * cond);
static int s_userlist(const char *, int,
		      lwmon_parsedata_t *, lwmon_condstore_t * cond);

static lwmon_parse_t config_1[] = {
    { "cmd_measure", 2,   1,   s_cmd_measure,      c_check,
      { lwmon_check_freq }},
    { "cmd_reopen",  1,   1,   s_cmd_reopen,       NULL,
      { lwmon_check_freq, }},
    { "cmd_terminate", 0, 0,   s_cmd_terminate,    NULL,
      { NULL, }},
    { "cpu",         2,   2,   s_cpu,              NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "disk",        2,   3,   s_disk,             lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, c_disktype, }},
    { "else",        0,   0,   lwmon_cond_else,    NULL,
      { NULL, }},
    { "elsif",       2,   1,   s_elsif,            lwmon_check_string,
      { c_condition, }},
    { "endif",       0,   0,   lwmon_cond_endif,   NULL,
      { NULL, }},
    { "file",        3,   2,   s_file,             lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, }},
    { "if",          2,   1,   s_if,               lwmon_check_string,
      { c_condition, }},
    { "hostname",    1,   1,   s_hostname,         NULL,
      { lwmon_check_string, }},
    { "listen",      4,   3,   s_listen,           c_port,
      { lwmon_check_string, lwmon_check_freq, lwmon_check_proto, }},
    { "load",        2,   2,   s_load,             NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "memory",      2,   2,   s_memory,           NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "network",     2,   2,   s_network,          lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, }},
    { "nonempty",    3,   2,   s_nonempty,         lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, }},
    { "print",       1,   3,   s_print,            NULL,
      { lwmon_check_file_or_stdout, c_append, c_format }},
    { "program",     1,   0,   s_program,          lwmon_check_string,
      { lwmon_check_program }},
    { "procs",       2,   2,   s_process,          lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, }},
    { "processes",   2,   2,   s_process,          lwmon_check_string,
      { lwmon_check_string, lwmon_check_freq, }},
    { "receive",     1,   0,   s_receive,          lwmon_check_string,
      { NULL, }},
    { "self",        2,   2,   s_self,             NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "send",        1,   0,   s_send,             lwmon_check_string,
      { NULL, }},
    { "usercount",   2,   2,   s_usercount,        NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "userlist",    2,   2,   s_userlist,         NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { "users",       2,   2,   s_usercount,        NULL,
      { lwmon_check_string, lwmon_check_freq, }},
    { NULL, }
};

static const lwmon_parse_t * config[] = {
    config_1,
    NULL
};

static int parse_opt_c(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    if (! lwmon_parse_file(arg, config))
	return -1;
    return 0;
}

static int parse_opt_d(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    if (! lwmon_parse_dir(arg, config))
	return -1;
    return 0;
}

static int parse_opt_n(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    lwmon_packet_hostname(arg);
    return 0;
}

static int parse_opt_o(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    if (! lwmon_parse_line(arg, config))
	return -1;
    return 0;
}

static int store_receive_fd(const char * host, int fd) {
    if (n_sends >= MAX_RECEIVES) {
	fprintf(stderr, "Too many network sources\n");
	return 0;
    }
    receives[n_receives].fd = fd;
    receives[n_receives].events = POLLIN;
    if (host) {
	receive_names[n_receives] = strdup(host);
	if (! receive_names[n_receives]) {
	    perror("malloc");
	    return 0;
	}
    } else {
	receive_names[n_receives] = NULL;
    }
    n_receives++;
    return 1;
}

static int store_send_fd(int fd) {
    if (n_sends >= MAX_SENDS) {
	fprintf(stderr, "Too many destinations\n");
	return 0;
    }
    sends[n_sends++] = fd;
    return 1;
}

static int store_print(const char * name, int append, int format) {
    print_t * np;
    FILE * F;
    np = malloc(sizeof(print_t) + strlen(name) + 1);
    if (! np) {
	perror("malloc");
	return 0;
    }
    if (strcmp(name, "-") == 0) {
	F = stdout;
    } else {
	F = fopen(name, append ? "a" : "w");
	if (! F) {
	    perror(name);
	    free(np);
	    return 0;
	}
    }
    np->next = prints;
    np->append = append;
    np->format = format;
    np->F = F;
    strcpy(np->name, name);
    prints = np;
    return 1;
}

static int store_receive(const char * host, const char * port) {
    if (port) {
	struct addrinfo hints, * res;
	int code, pass;
	memset(&hints, 0, sizeof(hints));
#ifdef AI_V4MAPPED
	hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG | AI_PASSIVE;
#else
	hints.ai_flags = AI_ADDRCONFIG | AI_PASSIVE;
#endif
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = 0;
	code = getaddrinfo(host, port, &hints, &res);
	if (code) {
	    fprintf(stderr, "%s:%s: %s\n", host, port, gai_strerror(code));
	    return 0;
	}
	/* we need to first bind IPv6 and then the rest to prevent an
	 * "address already in use" when binding to * or to both
	 * 0.0.0.0/0 and [::]/0 */
	for (pass = 0; pass < 2; pass++) {
	    struct addrinfo * run;
	    for (run = res; run; run = run->ai_next) {
		int fd, reuse = 1;
		if (pass == 0 ? run->ai_family != AF_INET6
			      : run->ai_family == AF_INET6)
		    continue;
		fd = socket(run->ai_family, run->ai_socktype, run->ai_protocol);
		if (fd < 0) goto error1;
		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
		if (bind(fd, run->ai_addr, run->ai_addrlen) < 0) goto error1;
		if (store_receive_fd(NULL, fd)) continue;
	    error1:
		if (host)
		    fprintf(stderr, "%s:%s: %s\n", host, port, strerror(errno));
		else
		    fprintf(stderr, "%s: %s\n", port, strerror(errno));
		if (fd >= 0) close(fd);
		freeaddrinfo(res);
		return 0;
	    }
	}
	freeaddrinfo(res);
	return 1;
    } else {
	struct sockaddr_un in_socket = {
	    .sun_family = AF_UNIX,
	    .sun_path   = "",
	};
	int fd, reuse = 1, remove = 0;
	if (strlen(host) >= sizeof(in_socket.sun_path)) {
	    fprintf(stderr, "Receive socket name too long\n");
	    return 0;
	}
	strcpy(in_socket.sun_path, host);
	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0) goto error2;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
	if (bind(fd, (struct sockaddr *)&in_socket, sizeof(in_socket)) < 0)
	    goto error2;
	remove = 1;
	if (store_receive_fd(host, fd)) return 1;
    error2:
	perror(host);
	if (fd >= 0) close(fd);
	if (remove) unlink(host);
	return 0;
    }
}

static int store_send(const char * host, const char * port) {
    if (port) {
	struct addrinfo hints, * res, * run;
	int code;
	memset(&hints, 0, sizeof(hints));
#ifdef AI_V4MAPPED
	hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
#else
	hints.ai_flags = AI_ADDRCONFIG;
#endif
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	code = getaddrinfo(host, port, &hints, &res);
	if (code) {
	    fprintf(stderr, "%s:%s: %s\n", host, port, gai_strerror(code));
	    return 0;
	}
	for (run = res; run; run = run->ai_next) {
	    int fd = socket(run->ai_family, run->ai_socktype, run->ai_protocol);
	    if (fd < 0) goto error1;
	    if (connect(fd, run->ai_addr, run->ai_addrlen) < 0) goto error1;
	    if (store_send_fd(fd)) continue;
	error1:
	    fprintf(stderr, "%s:%s: %s\n", host, port, strerror(errno));
	    if (fd >= 0) close(fd);
	    freeaddrinfo(res);
	    return 0;
	}
	freeaddrinfo(res);
	return 1;
    } else {
	struct sockaddr_un in_socket = {
	    .sun_family = AF_UNIX,
	    .sun_path   = "",
	};
	int fd;
	if (strlen(host) >= sizeof(in_socket.sun_path)) {
	    fprintf(stderr, "Output socket name too long\n");
	    return 0;
	}
	strcpy(in_socket.sun_path, host);
	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0) goto error2;
	if (connect(fd, (struct sockaddr *)&in_socket, sizeof(in_socket)) < 0)
	    goto error2;
	if (store_send_fd(fd)) return 1;
    error2:
	perror(host);
	return 0;
    }
}

static int parse_receive(const char * optname, const char * arg) {
    if (arg[0] == '/') {
	return store_receive(arg, NULL);
    } else {
	const char * colon = strchr(arg, ':');
	if (colon) {
	    int hostlen = colon - arg;
	    char host[1 + hostlen];
	    strncpy(host, arg, hostlen);
	    host[hostlen] = 0;
	    return store_receive(host, colon + 1);
	} else {
	    return store_receive(NULL, arg);
	}
    }
}

static int parse_send(const char * optname, const char * arg) {
    if (arg[0] == '/') {
	return store_send(arg, NULL);
    } else {
	const char * colon = strchr(arg, ':');
	if (colon) {
	    int hostlen = colon - arg;
	    char host[1 + hostlen];
	    strncpy(host, arg, hostlen);
	    host[hostlen] = 0;
	    return store_send(host, colon + 1);
	} else {
	    fprintf(stderr,
		    "Invalid argument to %s, need HOST:PORT or PATH\n",
		    optname);
	    return 0;
	}
    }
}

static int parse_opt_a(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    return store_print(arg, 1, 0) ? 0 : -1;
}

static int parse_opt_A(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    return store_print(arg, 1, 1) ? 0 : -1;
}

static int parse_opt_p(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    return store_print(arg, 0, 0) ? 0 : -1;
}

static int parse_opt_P(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    return store_print(arg, 0, 1) ? 0 : -1;
}

static int parse_opt_r(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    char optname[3] = { '-', opt, 0 };
    return parse_receive(optname, arg) ? 0 : -1;
}

static int parse_opt_s(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    char optname[3] = { '-', opt, 0 };
    return parse_send(optname, arg) ? 0 : -1;
}

static int parse_opt_S(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    return store_print(arg, 0, 2) ? 0 : -1;
}

static int parse_opt_t(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    if (strcasecmp(arg, "GMT") == 0) {
	time_format = lwmon_time_gmt;
	return 0;
    }
    if (strcasecmp(arg, "LOCAL") == 0) {
	time_format = lwmon_time_local;
	return 0;
    }
    if (strcasecmp(arg, "RAW") == 0) {
	time_format = lwmon_time_raw;
	return 0;
    }
    fprintf(stderr, "Invalid time FORMAT: %s\n", arg);
    return -1;
}

static int c_append(const char * kw, int num,
		    const char * arg, lwmon_parsedata_t * D)
{
    if (strcmp(arg, "append") == 0 || strcmp(arg, "a") == 0) {
	D->iv = 1;
    } else if (strcmp(arg, "overwrite") == 0 || strcmp(arg, "o") == 0) {
	D->iv = 0;
    } else {
	fprintf(stderr, "Invalid value for %s: %s\n", kw, arg);
	return 0;
    }
    return 1;
}

static int c_check(const char * kw, int num,
		   const char * arg, lwmon_parsedata_t * D)
{
    check_t * rc;
    for (rc = checks; rc; rc = rc->next) {
	if (strcmp(rc->name, arg) == 0) {
	    D->vp = rc;
	    return 1;
	}
    }
    fprintf(stderr, "Invalid check name for %s: %s\n", kw, arg);
    return 0;
}

static int c_condition(const char * kw, int num,
		       const char * arg, lwmon_parsedata_t * D)
{
    int i = lwmon_cond_find(condtypes, arg);
    if (i < 0) return 0;
    D->iv = i;
    return 1;
}

static int c_disktype(const char * kw, int num,
		      const char * arg, lwmon_parsedata_t * D)
{
    if (strcmp(arg, "mount") == 0 || strcmp(arg, "m") == 0) {
        D->iv = 1;
    } else if (strcmp(arg, "device") == 0 || strcmp(arg, "d") == 0) {
        D->iv = 2;
    } else if (strcmp(arg, "type") == 0 || strcmp(arg, "t") == 0) {
        D->iv = 3;
    } else {
        fprintf(stderr, "Invalid value for %s: %s\n", kw, arg);
        return 0;
    }
    return 1;
}

static int c_port(const char * kw, int num,
		  const char * arg, lwmon_parsedata_t * D)
{
    lwmon_parsedata_t * B = D - num;
    return lwmon_check_port(B[2].iv, arg, D);
}

static int c_format(const char * kw, int num,
		    const char * arg, lwmon_parsedata_t * D)
{
    if (strcmp(arg, "verbose") == 0 || strcmp(arg, "v") == 0) {
	D->iv = 1;
    } else if (strcmp(arg, "details") == 0 || strcmp(arg, "d") == 0) {
	D->iv = 1;
    } else if (strcmp(arg, "terse") == 0 || strcmp(arg, "t") == 0) {
	D->iv = 0;
    } else if (strcmp(arg, "binary") == 0 || strcmp(arg, "b") == 0) {
	D->iv = 2;
    } else {
	fprintf(stderr, "Invalid value for %s: %s\n", kw, arg);
	return 0;
    }
    return 1;
}

static int s_hostname(const char * kw, int argc,
		      lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (lwmon_if(cond))
	lwmon_packet_hostname(D[0].sp);
    return 1;
}

static int s_if(const char * kw, int argc,
		lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return lwmon_cond_if(&condtypes[D[0].iv], kw, argc - 1, D + 1, cond);
}

static int s_elsif(const char * kw, int argc,
		   lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return lwmon_cond_elsif(&condtypes[D[0].iv], kw, argc - 1, D + 1, cond);
}

static void send_packet(lwmon_packet_t * P) {
    print_t * prn;
    int i;
    if (P->data_count < 1) return;
    lwmon_closepacket(P);
    for (i = 0; i < n_sends; i++)
	lwmon_sendpacket(sends[i], P);
    for (prn = prints; prn; prn = prn->next)
	if (prn->format > 1)
	    lwmon_sendpacket(fileno(prn->F), P);
	else
	    lwmon_printpacket(prn->F, P, time_format, prn->format);
    lwmon_reinitpacket(P);
}

static void add_data(lwmon_packet_t * P, const lwmon_dataitem_t * D) {
    int ok = lwmon_add_data(P, D);
    if (ok) return;
    /* ok == 0 means it did not fit, but it could fit on a new packet */
    send_packet(P);
    lwmon_add_data(P, D);
}

static inline int64_t timeval_diff(const struct timeval * end,
				   const struct timeval * start)
{
    struct timeval diff;
    timersub(end, start, &diff);
    return diff.tv_sec * 1000 + diff.tv_usec / 1000;
}

static inline int64_t timespec_diff(const struct timespec * end,
				    const struct timespec * start)
{
    /* there doesn't appear to be a function equivalent to
     * timersub() for struct timespec */
    struct timespec diff;
    if (end->tv_nsec < start->tv_nsec) {
	diff.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
	diff.tv_sec = end->tv_sec - 1 - start->tv_sec;
    } else {
	diff.tv_nsec = end->tv_nsec - start->tv_nsec;
	diff.tv_sec = end->tv_sec - start->tv_sec;
    }
    return diff.tv_sec * 1000 + diff.tv_nsec / 1000000;
}

static inline int timespec_le(const struct timespec * a,
			      const struct timespec * b)
{
    return a->tv_sec < b->tv_sec ||
	   (a->tv_sec == b->tv_sec && a->tv_nsec <= b->tv_nsec);
}

static check_result_t cpu_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    cpu_t *prev, this;
    if (lwmon_os_cpu(&this.cpu) < 0) return CHECK_RETRY;
    if (clock_gettime(CLOCK_MONOTONIC, &this.when) < 0) return CHECK_RETRY;
    prev = C->extra;
    if (prev->when.tv_sec > 0) {
	int64_t msec;
	msec = timespec_diff(&this.when, &prev->when);
	D.type = C->type;
	D.name = C->name;
	D.namelen = C->namelen;
	D.parm = NULL;
	D.parmlen = 0;
	D.n_values = 3;
	D.values[0] = (this.cpu.user - prev->cpu.user) * 1000 / tick;
	D.values[1] = (this.cpu.system - prev->cpu.system) * 1000 / tick;
	D.values[2] = msec;
	add_data(P, &D);
    }
    *prev = this;
    return CHECK_CONTINUE;
}

static check_result_t disk_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    const char ** pv = C->data;
    int n;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.n_values = 9;
    for (n = 0; n < C->data_count; n++) {
	struct statvfs sbuff;
	if (statvfs(pv[n], &sbuff) < 0) continue;
	D.parm = pv[n];
	D.parmlen = strlen(D.parm);
	D.values[0] = sbuff.f_bsize;
	D.values[1] = sbuff.f_blocks;
	D.values[2] = sbuff.f_blocks - sbuff.f_bfree;
	D.values[3] = sbuff.f_bfree;
	D.values[4] = sbuff.f_bavail;
	D.values[5] = sbuff.f_files;
	D.values[6] = sbuff.f_files - sbuff.f_ffree;
	D.values[7] = sbuff.f_ffree;
	D.values[8] = sbuff.f_favail;
	add_data(P, &D);
    }
    return CHECK_CONTINUE;
}

static check_result_t file_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    const char ** pv = C->data;
    int ok = lwmon_has_file1(C->data_count, pv, C->flags);
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = ok ? pv[ok - 1] : "";
    D.parmlen = strlen(D.parm);
    D.n_values = 1;
    D.values[0] = ok ? 1 : 0;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static int add_port(lwmon_protocol_t proto, int port, void * _V) {
    port_t * V = _V;
    int i;
    for (i = 0; i < V->n_ports; i++)
	if (V->ports[i] == port)
	    V->counts[i]++;
    return 1;
}

static check_result_t listen_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    port_t V;
    int i;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = C->data_count;
    for (i = 0; i < C->data_count; i++)
	D.values[i] = 0;
    V.n_ports = C->data_count;
    V.ports = C->data;
    V.counts = D.values;
    if (lwmon_os_listen_list(C->flags, add_port, &V) < 0) return CHECK_RETRY;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static check_result_t load_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    lwmon_os_load_t L;
    if (lwmon_os_load(&L) < 0) return CHECK_RETRY;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = L.processors > 0 ? 4 : 3;
    D.values[0] = 100.0 * L.loads[0] + 0.5;
    D.values[1] = 100.0 * L.loads[1] + 0.5;
    D.values[2] = 100.0 * L.loads[2] + 0.5;
    D.values[3] = L.processors;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static check_result_t memory_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    lwmon_os_memory_t M;
    if (lwmon_os_memory(&M) < 0) return CHECK_RETRY;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = 7;
    D.values[0] = M.page_size;
    D.values[1] = M.mem_total;
    D.values[2] = M.mem_used;
    D.values[3] = M.mem_free;
    D.values[4] = M.swap_total;
    D.values[5] = M.swap_used;
    D.values[6] = M.swap_free;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static int net_search(const void * _name, const void * _elem) {
    const char * name = _name, * const * elem = _elem;
    return strcmp(name, *elem);
}

static int check_interface(const char * name, int64_t sent,
			   int64_t rcvd, void * _M)
{
    network_check_t * M = _M;
    const char ** I = bsearch(name, M->names, M->n_interfaces,
			      sizeof(const char *), net_search);
    if (! I) return 1;
    int n = I - M->names;
    M->interfaces[n].this.sent = sent;
    M->interfaces[n].this.rcvd = rcvd;
    if (M->interfaces[n].prev.sent >= 0 && M->interfaces[n].prev.rcvd >= 0) {
	M->D.parm = name;
	M->D.parmlen = strlen(name);
	M->D.values[0] = timespec_diff(&M->interfaces[n].this.check,
				       &M->interfaces[n].prev.check);
	M->D.values[1] = M->interfaces[n].this.sent
		       - M->interfaces[n].prev.sent;
	M->D.values[2] = M->interfaces[n].this.rcvd
		       - M->interfaces[n].prev.rcvd;
	add_data(M->P, &M->D);
    }
    M->interfaces[n].prev = M->interfaces[n].this;
    return 1;
}

static check_result_t network_run(lwmon_packet_t * P, const check_t * C) {
    network_check_t M;
    struct timespec now;
    int n;
    clock_gettime(CLOCK_MONOTONIC, &now);
    M.n_interfaces = C->data_count;
    M.interfaces = C->extra;
    M.names = C->data;
    M.P = P;
    M.D.type = C->type;
    M.D.name = C->name;
    M.D.namelen = C->namelen;
    M.D.n_values = 3;
    for (n = 0; n < M.n_interfaces; n++)
	M.interfaces[n].this.check = now;
    lwmon_os_interface_list(check_interface, &M);
    return CHECK_CONTINUE;
}

static int add_process(pid_t pid, const char * name, void * _pr) {
    process_t * pr = _pr;
    if (pr->n_patterns < 1) {
	pr->count[0]++;
    } else {
	int i;
	for (i = 0; i < pr->n_patterns; i++)
	    if (fnmatch(pr->patterns[i], name, 0) == 0)
		pr->count[i]++;
    }
    return 0;
}

static check_result_t process_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    process_t pr;
    int i, dc = C->data_count > 0 ? C->data_count : 1;
    for (i = 0; i < dc; i++)
	D.values[i] = 0;
    pr.n_patterns = C->data_count;
    pr.patterns = C->data;
    pr.count = D.values;
    if (lwmon_os_process_list(add_process, &pr) < 0) return CHECK_RETRY;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = dc;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static check_result_t program_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    struct timespec ended;
    struct rusage ru1, ru2;
    program_run_t * pr = C->extra;
    int status, we;
    if (pr->pid < 0) {
	/* we haven't started the program yet */
	pid_t pid = fork();
	if (pid < 0) {
	    pr->pid--;
	    if (pr->pid < -5) return CHECK_STOP;
	    /* we'll retry next time */
	    return CHECK_RETRY;
	}
	if (pid == 0) {
	    /* child process */
	    char * argv[C->data_count + 1];
	    char ** pv = C->data;
	    int i;
	    for (i = 0; i < C->data_count; i++)
		argv[i] = pv[i];
	    argv[C->data_count] = NULL;
	    fclose(stdin);
	    fclose(stdout);
	    fclose(stderr);
	    i = open("/dev/null", O_RDWR);
	    if (i > 0) dup2(i, 0);
	    if (i >= 0 && i != 1) dup2(i, 1);
	    if (i >= 0 && i != 2) dup2(i, 2);
	    execv(argv[0], argv);
	    exit(255);
	}
	/* parent process */
	pr->pid = pid;
	clock_gettime(CLOCK_MONOTONIC, &pr->started);
	return CHECK_CONTINUE;
    }
    /* is it still running */
    if (getrusage(RUSAGE_CHILDREN, &ru1) < 0) return CHECK_RETRY;
    we = waitpid(pr->pid, &status, WNOHANG);
    if (we == 0) return CHECK_RETRY;
    if (we < 0) return CHECK_STOP; /* "something went wrong" */
    if (getrusage(RUSAGE_CHILDREN, &ru2) < 0) return CHECK_STOP; /* sorry */
    if (clock_gettime(CLOCK_MONOTONIC, &ended) < 0) return CHECK_STOP; /* sorry */
    /* note that we are single-threaded, so no other child process has been
     * waited for between the two calls to getrusage; therefore the difference
     * between them is the actual resource usage of pr->pid; this is the
     * reason we cannot call wait() in the signal handler: getrusage is
     * not listed as async-safe so we cannot call it there */
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = 4;
    D.values[0] = timespec_diff(&ended, &pr->started);
    D.values[1] = timeval_diff(&ru2.ru_utime, &ru1.ru_utime);
    D.values[2] = timeval_diff(&ru2.ru_stime, &ru1.ru_stime);
    D.values[3] = status;
    add_data(P, &D);
    return CHECK_STOP;
}

static check_result_t usercount_run(lwmon_packet_t * P, const check_t * C) {
    lwmon_dataitem_t D;
    struct utmpx *ut;
    int count = 0;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = 1;
    setutxent();
    while ((ut = getutxent()) != NULL)
	if (ut->ut_type == USER_PROCESS)
	    count++;
    endutxent();
    D.values[0] = count;
    add_data(P, &D);
    return CHECK_CONTINUE;
}

static check_result_t userlist_run(lwmon_packet_t * P, const check_t * C) {
    char nbuff[MAX_USERLEN], * names[MAX_USERS];
    int count[MAX_USERS];
    lwmon_dataitem_t D;
    struct utmpx *ut;
    int nlen = 0, n, n_users = 0;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    setutxent();
    while ((ut = getutxent()) != NULL) {
	int found = 0;
	if (ut->ut_type != USER_PROCESS) continue;
	for (n = 0; n < n_users && ! found; n++) {
	    if (strcmp(names[n], ut->ut_user) != 0) continue;
	    count[n]++;
	    found = 1;
	}
	if (! found) {
	    /* gcc will complain and break things if we use strlen, even
	     * though ut->ut_user is NUL-terminated; strcpy doesn't complain
	     * today but it will certainly complain on a newer version of gcc
	     * including more misleading warnings and errors */
	    int sl = strnlen(ut->ut_user, UT_NAMESIZE), l = 1 + sl;
	    if (nlen + l > MAX_USERLEN || n_users >= MAX_USERS)
		continue;
	    names[n_users] = nbuff + nlen;
	    strncpy(names[n_users], ut->ut_user, sl);
	    names[n_users][sl] = 0;
	    count[n_users] = 1;
	    n_users++;
	    nlen += l;
	}
    }
    endutxent();
    if (n_users == 0) {
	names[0] = "";
	count[0] = 0;
	n_users = 1;
    }
    D.n_values = 1;
    for (n = 0; n < n_users; n++) {
	D.parm = names[n];
	D.parmlen = strlen(names[n]);
	D.values[0] = count[n];
	add_data(P, &D);
    }
    return CHECK_CONTINUE;
}

static check_result_t self_run(lwmon_packet_t * P, const check_t * C) {
    struct rusage rs;
    lwmon_dataitem_t D;
    self_t * S = C->data, N;
    int64_t td;
    if (getrusage(RUSAGE_SELF, &rs) < 0) return CHECK_RETRY;
    if (clock_gettime(CLOCK_MONOTONIC, &N.when) < 0) return CHECK_RETRY;
    D.type = C->type;
    D.name = C->name;
    D.namelen = C->namelen;
    D.parm = NULL;
    D.parmlen = 0;
    D.n_values = 4;
    N.utime = rs.ru_utime;
    N.stime = rs.ru_stime;
    N.majflt = rs.ru_majflt;
    td = timespec_diff(&N.when, &S->when);
    if (td < 1) td = 1;
    D.values[0] = timeval_diff(&N.utime, &S->utime) * 1000 / td;
    D.values[1] = timeval_diff(&N.stime, &S->stime) * 1000 / td;
    D.values[2] = rs.ru_maxrss;
    D.values[3] = (int64_t)(N.majflt - S->majflt) * 1000 / td;
    add_data(P, &D);
    *S = N;
    return CHECK_CONTINUE;
}

static int store_check(lwmon_parsedata_t * D, lwmon_condstore_t * cond,
		       int extra, int flags, const char * type,
		       check_result_t (*run)(lwmon_packet_t *, const check_t *))
{
    check_t * check;
    if (! lwmon_if(cond)) return 1;
    check = malloc(sizeof(check_t) + extra);
    if (! check) {
	perror("malloc");
	return 0;
    }
    check->next = checks;
    check->freq = D[1].iv;
    check->is_running = 1;
    check->is_program = 0;
    check->re_run = 0;
    check->name = strdup(D[0].sp);
    if (! check->name) {
	perror("malloc");
	free(check);
	return 0;
    }
    check->namelen = strlen(D[0].sp);
    check->flags = flags;
    check->run = run;
    check->type = lwmon_datatype(type);
    check->data = &check[1];
    check->extra = &check[1];
    check->data_count = 0;
    checks = check;
    return 1;
}

static int store_patterns(int argc, lwmon_parsedata_t * D,
			  lwmon_condstore_t * cond, int extra, int flags,
			  const char * type,
			  check_result_t (*run)(lwmon_packet_t *, const check_t *))
{
    int sp = 0, sd = 0, i;
    char ** pv, * dv;
    if (! lwmon_if(cond)) return 1;
    if (argc > 2 + LWMON_ITEMS) {
	fprintf(stderr, "Too many patterns\n");
	return 0;
    }
    for (i = 2; i < argc; i++) {
	sp += sizeof(const char *);
	sd += + 1 + strlen(D[i].sp);
    }
    if (! store_check(D, cond, extra + sp + sd, flags, type, run))
	return 0;
    checks->data_count = argc - 2;
    dv = checks->data;
    if (extra) {
	dv += extra;
	checks->data = dv;
    }
    pv = checks->data;
    dv += sp;
    for (i = 2; i < argc; i++) {
	pv[i - 2] = dv;
	strcpy(dv, D[i].sp);
	dv += 1 + strlen(dv);
    }
    return 1;
}

static int add_disk(const char * fstype, const char * device,
		    const char * mountpoint, void * _M)
{
    pattern_t * M = _M;
    const char * match = NULL;
    int n, add_it = M->n_patterns < 4;
    if (! add_it) {
	switch (M->patterns[2].iv) {
	    case 1 : match = mountpoint; break;
	    case 2 : match = device; break;
	    case 3 : match = fstype; break;
	}
	if (! match) return 1;
    }
    for (n = 3; ! add_it && n < M->n_patterns; n++)
	if (fnmatch(M->patterns[n].sp, match, 0) == 0)
	    add_it = 1;
    if (! add_it) return 1;
    for (n = 2; n < M->n_results; n++)
	if (strcmp(M->results[n].sp, mountpoint) == 0)
	    return 1;
    if (M->n_results >= MAX_RESULTS) {
	fprintf(stderr, "Too many mount points match\n");
	return 0;
    }
    M->results[M->n_results].sp = strdup(mountpoint);
    if (! M->results[M->n_results].sp) {
	perror("malloc");
	return 0;
    }
    M->n_results++;
    return 1;
}

static int s_cmd_measure(const char * kw, int argc,
			 lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int i;
    if (! lwmon_if(cond)) return 1;
    for (i = 1; i < argc; i++) {
	check_t * rc = D[i].vp;
	if (rc->re_run <= 0 || rc->re_run > D[0].iv)
	    rc->re_run = D[0].iv;
    }
    return 1;
}

static int s_cmd_reopen(const char * kw, int argc,
			lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! lwmon_if(cond)) return 1;
    cmd_reopen_ok = 1000 * D[0].iv;
    return 1;
}

static int s_cmd_terminate(const char * kw, int argc,
			   lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! lwmon_if(cond)) return 1;
    cmd_terminate_ok = 1;
    return 1;
}

static int s_cpu(const char * kw, int argc,
		 lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    cpu_t * P;
    if (! lwmon_if(cond)) return 1;
    if (! store_check(D, cond, sizeof(cpu_t), 0, "cpu", cpu_run))
	return 0;
    tick = sysconf(_SC_CLK_TCK);
    P = checks->extra;
    P->when.tv_sec = 0;
    return 1;
}

static int s_disk(const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    pattern_t M;
    int ok, n;
    if (! lwmon_if(cond)) return 1;
    M.patterns = D;
    M.n_patterns = argc;
    M.n_results = 2;
    M.results[0].sp = D[0].sp;
    M.results[1].iv = D[1].iv;
    /* convert patterns to list of mount points */
    lwmon_os_disk_list(add_disk, &M);
    if (M.n_results < 3) {
	fprintf(stderr, "No disks matched\n");
	return 0;
    }
    ok = store_patterns(M.n_results, M.results, cond, 0, 0, "disk", disk_run);
    for (n = 2; n < M.n_results; n++)
	free(M.results[n].sp);
    return ok;
}

static int s_file(const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_patterns(argc, D, cond, 0, 0, "file", file_run);
}

static int s_listen(const char * kw, int argc,
		    lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int * pv, i;
    if (! lwmon_if(cond)) return 1;
    if (argc > LWMON_VALUES + 2)
	argc = LWMON_VALUES + 2;
    if (! store_check(D, cond, sizeof(int) * (argc - 3),
		      D[2].iv, "listen", listen_run))
	return 0;
    checks->data_count = argc - 3;
    pv = checks->data;
    for (i = 3; i < argc; i++)
	pv[i - 3] = D[i].iv;
    return 1;
}

static int s_load(const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_check(D, cond, 0, 0, "load", load_run);
}

static int s_memory(const char * kw, int argc,
		    lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_check(D, cond, 0, 0, "memory", memory_run);
}

static int add_interface(const char * name, int64_t sent,
			 int64_t rcvd, void * _M)
{
    pattern_t * M = _M;
    int n, add_it;
    add_it = M->n_patterns < 3;
    for (n = 2; ! add_it && n < M->n_patterns; n++)
	if (fnmatch(M->patterns[n].sp, name, 0) == 0)
	    add_it = 1;
    if (! add_it) return 1;
    for (n = 2; n < M->n_results; n++)
	if (strcmp(M->results[n].sp, name) == 0)
	    return 1;
    if (M->n_results >= MAX_RESULTS) {
	fprintf(stderr, "Too many interfaces match\n");
	return 0;
    }
    M->results[M->n_results].sp = strdup(name);
    if (! M->results[M->n_results].sp) {
	perror("malloc");
	return 0;
    }
    M->n_results++;
    return 1;
}

static int net_sort(const void * _a, const void * _b) {
    const lwmon_parsedata_t * a = _a, * b = _b;
    return strcmp(a->sp, b->sp);
}

static int s_network(const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    pattern_t M;
    int ok, n;
    if (! lwmon_if(cond)) return 1;
    M.patterns = D;
    M.n_patterns = argc;
    M.n_results = 2;
    M.results[0].sp = D[0].sp;
    M.results[1].iv = D[1].iv;
    /* convert patterns to list of network interfaces */
    lwmon_os_interface_list(add_interface, &M);
    if (M.n_results < 3) {
	fprintf(stderr, "No interfaces matched\n");
	return 0;
    }
    qsort(M.results + 2, M.n_results - 2, sizeof(lwmon_parsedata_t), net_sort);
    ok = store_patterns(M.n_results, M.results, cond,
			M.n_results * sizeof(network_t), 0,
			"network", network_run);
    if (ok) {
	network_t * extra = checks->extra;
	for (n = 0; n < M.n_results; n++)
	    extra[n].prev.sent = extra[n].prev.rcvd = -1;
    }
    for (n = 2; n < M.n_results; n++)
	free(M.results[n].sp);
    return ok;
}

static int s_nonempty(const char * kw, int argc,
		      lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_patterns(argc, D, cond, 0, 1, "file", file_run);
}

static int s_process(const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_patterns(argc, D, cond, 0, 0, "processes", process_run);
}

static int s_program(const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int ok;
    if (! lwmon_if(cond)) return 1;
    ok = store_patterns(argc, D, cond, sizeof(program_run_t), 0,
			"program", program_run);
    if (ok) {
	program_run_t * pr = checks->extra;
	pr->pid = -1;
	checks->is_program = 1;
    }
    return ok;
}

static int s_usercount(const char * kw, int argc,
		       lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_check(D, cond, 0, 0, "usercount", usercount_run);
}

static int s_userlist(const char * kw, int argc,
		      lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    return store_check(D, cond, 0, 0, "userlist", userlist_run);
}

static int s_self(const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    self_t *S;
    if (! lwmon_if(cond)) return 1;
    if (! store_check(D, cond, sizeof(*S), 0, "self", self_run))
	return 0;
    S = checks->data;
    if (clock_gettime(CLOCK_MONOTONIC, &S->when) < 0) {
	perror("clock_gettime");
	return 0;
    }
    S->utime.tv_sec = S->stime.tv_sec = 0;
    S->utime.tv_usec = S->stime.tv_usec = 0;
    S->majflt = 0;
    return 1;
}

static int s_print(const char * kw, int argc,
		   lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    if (! lwmon_if(cond)) return 1;
    return store_print(D[0].sp, argc > 1 ? D[1].iv : 0, argc > 2 ? D[2].iv : 0);
}

static int s_receive(const char * kw, int argc,
		     lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int n;
    if (! lwmon_if(cond)) return 1;
    for (n = 0; n < argc; n++)
	if (! parse_receive("receive", D[n].sp))
	    return 0;
    return 1;
}

static int s_send(const char * kw, int argc,
		  lwmon_parsedata_t * D, lwmon_condstore_t * cond)
{
    int n;
    if (! lwmon_if(cond)) return 1;
    for (n = 0; n < argc; n++)
	if (! parse_send("send", D[n].sp))
	    return 0;
    return 1;
}

static int reschedule_measures(const struct timespec * now) {
    check_t * rc;
    int change = 0;
    for (rc = checks; rc; rc = rc->next) {
	if (rc->re_run > 0 && timespec_diff(now, &rc->last_check) > rc->re_run) {
	    rc->next_check = 0;
	    change = 1;
	}
    }
    return change;
}

int main(int argc, char *argv[]) {
    struct sigaction sigact;
    lwmon_packet_t P;
    check_t * rc;
    struct timespec now, first;
    int i, any_running;
    lwmon_initops(argc, argv);
    if (! lwmon_opts(optlist) || lwmon_argcount() > 0) {
	lwmon_shortusage(stderr, "");
	return 1;
    }
    if (show_version || show_help) {
	if (show_version)
	    printf("lwmon 0.1 2021-12-19\n");
	if (show_version && show_help)
	    printf("\n");
	if (show_help)
	    lwmon_usage(stdout, "", optlist, helplist);
	return 0;
    }
    if (! checks && ! n_receives && ! readfile) {
	fprintf(stderr, "No checks or sources defined\n");
	return 2;
    }
    if (n_sends == 0 && prints == NULL) {
	fprintf(stderr, "No outputs defined\n");
	return 2;
    }
    if (readfile) {
	int fd = open(readfile, O_RDONLY);
	if (fd < 0) {
	    perror(readfile);
	    return 3;
	}
	while (lwmon_readpacket(fd, &P) > 0)
	    send_packet(&P);
	close(fd);
	return 0;
    }
    if (daemonise) {
	if (daemon(1, 0) < 0) {
	    perror("fork");
	    return 3;
	}
    }
    if (pidfile) {
	FILE * F = fopen(pidfile, "w");
	if (F) {
	    fprintf(F, "%d\n", (int)getpid());
	    fclose(F);
	}
    }
    clock_gettime(CLOCK_MONOTONIC, &now);
    last_reopen = now;
    first.tv_sec = 0;
    first.tv_nsec = 1000000;
    for (rc = checks; rc; rc = rc->next) {
	rc->last_check = now;
	rc->next_check = now.tv_sec + rc->freq;
	if (first.tv_sec == 0 || first.tv_sec > rc->next_check)
	    first.tv_sec = rc->next_check;
    }
    memset(&sigact, 0, sizeof(sigact));
    sigact.sa_handler = sig_stop;
    sigact.sa_flags = SA_RESETHAND;
    sigemptyset(&sigact.sa_mask);
    sigaction(SIGINT, &sigact, NULL);
    sigaction(SIGQUIT, &sigact, NULL);
    sigaction(SIGTERM, &sigact, NULL);
    sigact.sa_flags = 0;
    sigact.sa_handler = sig_hup;
    sigaction(SIGHUP, &sigact, NULL);
    sigact.sa_handler = sig_chld;
    sigaction(SIGCHLD, &sigact, NULL);
    while (! interrupt) {
	int td = 0;
	clock_gettime(CLOCK_MONOTONIC, &now);
	if (timespec_le(&now, &first)) td = timespec_diff(&first, &now);
	poll(receives, n_receives, td);
	/* note that we don't check for things like failing with EINTR here:
	 * if it's a child ending we'll want to handle that immediately
	 * and not at the check's frequency */
	if (interrupt) break;
	lwmon_initpacket(&P);
	first.tv_sec = 0;
	any_running = 0;
	clock_gettime(CLOCK_MONOTONIC, &now);
	for (rc = checks; rc; rc = rc->next) {
	    if (! rc->is_running) continue;
	    if (rc->next_check <= now.tv_sec) {
		rc->last_check = now;
		check_result_t res = rc->run(&P, rc);
		if (res == CHECK_STOP) {
		    check_t * cr;
		    int ar;
		    /* this check will no longer run; and if no other program
		     * is still running we enter "end mode" in which each
		     * check is disabled as soon as it runs */
		    rc->is_running = 0;
		    for (cr = checks, ar = 0; cr; cr = cr->next)
			if (cr->is_program && cr->is_running)
			    ar = 1;
		    if (! ar) end_mode = 1;
		}
		if (end_mode) {
		    rc->is_running = 0;
		    continue;
		}
		rc->next_check += res == CHECK_RETRY ? RETRY_TIME : rc->freq;
		if (rc->next_check <= now.tv_sec) rc->next_check = now.tv_sec + 1;
	    }
	    any_running = 1;
	    if (first.tv_sec == 0 || first.tv_sec > rc->next_check)
		first.tv_sec = rc->next_check;
	}
	send_packet(&P);
	i = 0;
	while (i < n_receives) {
	    if (receives[i].revents & POLLIN) {
		if (lwmon_receivepacket(receives[i].fd, &P, NULL, NULL) > 0) {
		    /* check for commands */
		    if (P.command) {
			if (P.command & lwmon_command_measure)
			    if (reschedule_measures(&now))
				first.tv_sec = now.tv_sec;
			if (cmd_reopen_ok >= 0 && (P.command & lwmon_command_reopen))
			    if (timespec_diff(&now, &last_reopen) >= cmd_reopen_ok)
				reopen = 1;
			if (cmd_terminate_ok && (P.command & lwmon_command_terminate))
			    interrupt = 1;
		    }
		    /* send packet on */
		    send_packet(&P);
		}
	    }
	    if (end_mode || receives[i].revents & POLLERR) {
		n_receives--;
		if (i != n_receives)
		    receives[i] = receives[n_receives];
	    } else {
		i++;
	    }
	}
	if (! any_running) {
	    first.tv_sec = now.tv_sec + 60;
	    if (interrupt || ! n_receives) break; /* no more checks left */
	}
	if (reopen) {
	    print_t * prn = prints;
	    last_reopen = now;
	    while (prn) {
		if (strcmp(prn->name, "-") == 0) {
		    prn = prn->next;
		    continue;
		}
		fclose(prn->F);
		/* unlike the initial open, we always append here to
		 * avoid overwriting on SIGHUP */
		prn->F = fopen(prn->name, "a");
		if (prn->F) {
		    prn = prn->next;
		} else {
		    print_t * go = prn;
		    prn = go->next;
		    free(go);
		}
	    }
	    reopen = 0;
	}
    }
    for (i = 0; i < n_receives; i++) {
	close(receives[i].fd);
	if (receive_names[i]) {
	    unlink(receive_names[i]);
	    free(receive_names[i]);
	}
    }
    for (i = 0; i < n_sends; i++)
	close(sends[i]);
    while (prints) {
	print_t * go = prints;
	if (strcmp(go->name, "-") != 0)
	    fclose(go->F);
	prints = go->next;
	free(go);
    }
    return 0;
}

