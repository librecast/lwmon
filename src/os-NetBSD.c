/* SPDX-License-Identifier: MIT
 *
 * system-dependent functions to retrieve monitoring data -- NetBSD
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include <lwmon/os.h>
#include <lwmon/options.h>

/* size of command name when reading process lists */
#define CMDBUFF 1024

/* determine system load and (if known) number of processors */
int lwmon_os_load(lwmon_os_load_t * L) {
    double loadavg[3];
    int ns = getloadavg(loadavg, sizeof(loadavg) / sizeof(double)), n;
    if (ns < 0) return -1;
    for (n = 0; n < 3; n++)
	L->loads[n] = n < ns ? loadavg[n] : 0.00;
    L->processors = sysconf(_SC_NPROCESSORS_ONLN);
    if (L->processors < 0) L->processors = 0;
    return 0;
}

/* determine memory/swap used, available, total; this is identical to
 * the Linux version as recent(ish) NetBSD's have a similar-looking
 * /proc/meminfo */
int lwmon_os_memory(lwmon_os_memory_t * M) {
    char line[128];
    FILE * F = fopen("/proc/meminfo", "r");
    long kb;
    if (! F) return -1;
    M->page_size = sysconf(_SC_PAGESIZE);
    if (M->page_size == -1) {
	int sve = errno;
	fclose(F);
	errno = sve;
	return -1;
    }
    kb = M->page_size / 1024;
    M->mem_total = -1;
    M->mem_used = -1;
    M->mem_free = -1;
    M->swap_total = -1;
    M->swap_used = -1;
    M->swap_free = -1;
    while (fgets(line, sizeof(line), F)) {
#define assign_val(kw, var) \
if (strncmp(line, kw, strlen(kw)) == 0 && line[strlen(kw)] == ':') { \
M->var = atoll(line + strlen(kw) + 1) / kb; \
continue; \
}
	assign_val("MemTotal", mem_total);
	assign_val("MemFree", mem_free);
	assign_val("SwapTotal", swap_total);
	assign_val("SwapFree", swap_free);
#undef assign_val
    }
    if (M->mem_total > 0 && M->mem_free >= 0)
	M->mem_used = M->mem_total - M->mem_free;
    if (M->swap_total > 0 && M->swap_free >= 0)
	M->swap_used = M->swap_total - M->swap_free;
    fclose(F);
    return 0;
}

/* determine CPU time used since some time in the past, in milliseconds */
int lwmon_os_cpu(lwmon_os_cpu_t * C) {
    char line[128];
    FILE * F = fopen("/proc/stat", "r");
    if (! F) return -1;
    while (fgets(line, sizeof(line), F)) {
	long long ut, nt, st;
	if (strncmp(line, "cpu ", 4) != 0) continue;
	if (sscanf(line + 4, "%lld%lld%lld", &ut, &nt, &st) < 3) continue;
	C->user = ut + nt;
	C->system = st;
	fclose(F);
	return 0;
    }
    fclose(F);
    errno = ENOENT;
    return -1;
}

/* call f(pid, processname, arg) for each running process */
int lwmon_os_process_list(int (*f)(pid_t, const char *, void *), void * arg) {
    char procname[32], cmdbuff[CMDBUFF];
    DIR * D = opendir("/proc");
    struct dirent * E;
    int i, ok = 0;
    if (! D) return -1;
    while (ok >= 0 && (E = readdir(D)) != NULL) {
	char * slash;
	ssize_t nr;
	pid_t pid;
	int go = 1;
	for (i = 0; E->d_name[i] && ok; i++)
	    if (! isdigit((int)(unsigned char)E->d_name[i]))
		go = 0;
	if (! go) continue;
	pid = atoi(E->d_name);
	sprintf(procname, "/proc/%d/exe", (int)pid);
	nr = readlink(procname, cmdbuff, sizeof(cmdbuff) - 1);
	if (nr <= 0) continue;
	cmdbuff[nr] = 0;
	slash = strrchr(cmdbuff, '/');
	ok = f(pid, slash ? (slash + 1) : cmdbuff, arg);
    }
    closedir(D);
    return ok;
}

/* call f(fstype, device_path, mount_point, arg) for each mount */
int lwmon_os_disk_list(int (*f)(const char *, const char *,
				const char *, void *),
		       void * arg)
{
    int ns = getvfsstat(NULL, 0, ST_NOWAIT), ok = -1;
    if (ns > 0) {
	struct statvfs buf[ns];
	ns = getvfsstat(buf, sizeof(buf), ST_NOWAIT);
	if (ns > 0) {
	    int n;
	    for (n = ok = 0; n < ns && ok >= 0; n++)
		ok = f(buf[n].f_fstypename, buf[n].f_mntfromname,
		       buf[n].f_mntonname, arg);
	}
    }
    return ok;
}

static inline void skip_space(char ** ptr) {
    while (*ptr && isspace((int)(unsigned char)**ptr)) (*ptr)++;
}

static inline void skip_nonspace(char ** ptr) {
    while (*ptr && ! isspace((int)(unsigned char)**ptr)) (*ptr)++;
}

static inline int skip_constant(char ** _ptr, const char * c, int l) {
    char * ptr = *_ptr;
    if (strncmp(ptr, c, l) != 0) return 0;
    ptr += l;
    if (ptr) {
	if (! isspace((int)(unsigned char)*ptr)) return 0;
	ptr++;
    }
    *_ptr = ptr;
    return 1;
}

/* call f(proto, port, arg) for all listening sockets matching the first
 * argument */
int lwmon_os_listen_list(lwmon_protocol_t proto,
			 int (*f)(lwmon_protocol_t, int, void *),
			 void * arg)
{
    // XXX it would be better to find a way to do this without using
    // XXX a pipe from sockstat
    char cmdbuff[128];
    FILE * F;
    const char * n1 = NULL, * n2 = NULL, * af = "46";
    lwmon_protocol_t p1 = proto, p2 = 0;
    int ok = -1, l1 = 0, l2 = 0, sve;
    switch (proto) {
	 case LWMON_PROTO_TCP4 :
	    n1 = "tcp";
	    af = "4";
	    break;
	 case LWMON_PROTO_TCP6 :
	    n1 = "tcp6";
	    af = "6";
	    break;
	 case LWMON_PROTO_TCP :
	    n1 = "tcp";
	    p1 = LWMON_PROTO_TCP4;
	    n2 = "tcp6";
	    p2 = LWMON_PROTO_TCP6;
	    af = "46";
	    break;
	 case LWMON_PROTO_UDP4 :
	    n1 = "udp";
	    af = "4";
	    break;
	 case LWMON_PROTO_UDP6 :
	    n1 = "udp6";
	    af = "6";
	    break;
	 case LWMON_PROTO_UDP :
	    n1 = "udp";
	    p1 = LWMON_PROTO_UDP4;
	    n2 = "udp6";
	    p2 = LWMON_PROTO_UDP6;
	    af = "46";
	    break;
    }
    if (! n1) return 0;
    l1 = strlen(n1);
    if (n2) l2 = strlen(n2);
    snprintf(cmdbuff, sizeof(cmdbuff), "sockstat -ln%s", af);
    F = popen(cmdbuff, "r");
    if (! F) return -1;
    if (fgets(cmdbuff, sizeof(cmdbuff), F)) {
	ok = 0;
	while (ok >= 0 && fgets(cmdbuff, sizeof(cmdbuff), F)) {
	    char * ptr = cmdbuff, * addr;
	    int port, mul;
	    lwmon_protocol_t p;
	    skip_space(&ptr);
	    /* skip USER field */
	    skip_nonspace(&ptr);
	    skip_space(&ptr);
	    /* skip COMMAND field */
	    skip_nonspace(&ptr);
	    skip_space(&ptr);
	    /* skip PID field */
	    skip_nonspace(&ptr);
	    skip_space(&ptr);
	    /* skip FD field */
	    skip_nonspace(&ptr);
	    skip_space(&ptr);
	    /* protocol field follows */
	    if (n1 && skip_constant(&ptr, n1, l1))
		p = p1;
	    else if (n2 && skip_constant(&ptr, n2, l2))
		p = p2;
	    else
		continue;
	    skip_space(&ptr);
	    /* local address */
	    addr = ptr;
	    skip_nonspace(&ptr);
	    ptr--;
	    port = 0;
	    mul = 1;
	    while (ptr >= addr && isdigit((int)(unsigned char)*ptr)) {
		port += mul * (*ptr - '0');
		mul *= 10;
		ptr--;
	    }
	    ok = f(p, port, arg);
	}
    }
    sve = errno;
    fclose(F);
    errno = sve;
    return ok;
}

/* call f(name, bytes_sent, bytes_recv, arg) for all network interface */
int lwmon_os_interface_list(int (*f)(const char *, int64_t, int64_t, void *),
			    void * arg)
{
    // XXX need to find a better way to do this, possibly using
    // XXX sysctl net.interfaces.* and/or ioctl(SIOCGIFDATA)
    char cmdbuff[128], ifname[IF_NAMESIZE] = "";
    FILE * F = popen("ifconfig -av", "r");
    int64_t sent = -1, rcvd = -1;
    int sve, ok = 0;
    if (! F) return -1;
    while (ok >= 0 && fgets(cmdbuff, sizeof(cmdbuff), F)) {
	char * ptr;
	int64_t bytes = 0, mul = 1;
	int is_in = 0, l;
	if (! isspace((int)(unsigned char)cmdbuff[0])) {
	    char * colon = strchr(cmdbuff, ':');
	    int l;
	    if (! colon) continue;
	    l = colon - cmdbuff;
	    cmdbuff[l] = 0;
	    strcpy(ifname, cmdbuff);
	    sent = rcvd = -1;
	    continue;
	}
	if (! ifname[0]) continue;
	ptr = cmdbuff;
	skip_space(&ptr);
	if (strncmp(ptr, "input:", 6) == 0)
	    is_in = 1;
	else if (strncmp(ptr, "output:", 7) == 0)
	    is_in = 0;
	else
	    continue;
	ptr = strstr(cmdbuff, "bytes");
	if (! ptr) continue;
	l = ptr - cmdbuff;
	while (l > 0 && isspace((int)(unsigned char)cmdbuff[l - 1])) l--;
	while (l > 0 && isdigit((int)(unsigned char)cmdbuff[l - 1])) {
	    l--;
	    bytes += mul * (cmdbuff[l] - '0');
	    mul *= 10;
	}
	if (is_in)
	    rcvd = bytes;
	else
	    sent = bytes;
	if (sent >= 0 && rcvd >= 0)
	    ok = f(ifname, sent, rcvd, arg);
    }
    sve = errno;
    fclose(F);
    errno = sve;
    return ok;
}

