/* SPDX-License-Identifier: MIT
 *
 * convert lwmon's binary data files to SQL statements to import
 * the data into a database
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <lwmon/options.h>
#include <lwmon/packet.h>

/* maximum number of extra fields which will go into the database update */
#define MAX_EXTRAS 32

static int show_help = 0, show_version = 0, num_extras = 0;
static const char * outfile = NULL, * table = "lwmon_data";
static const char * ts_field = "timestamp", * name_field = "name";
static const char * parm_field = "parm", * key_field = "key";
static const char * value_field = "value", * host_field = NULL;
static const char * extra_keys[MAX_EXTRAS], * extra_values[MAX_EXTRAS];
static int extra_keylen[MAX_EXTRAS];

static int parse_opt_e(char, const char *, int, char *[], lwmon_optdata_t);

static lwmon_opts_t options[] = {
    {  0,  "Output options", LWMON_OPT_TITLE, },
    { 'o', "FILE", LWMON_OPT_FILE, NULL, { .s = &outfile },
	   "Send output to file (default standard output)" },

    {  0,  "SQL options", LWMON_OPT_TITLE, },
    { 't', "TABLE", LWMON_OPT_STRING, NULL, { .s = &table },
	   "Name of table to update (default: lwmon_data)" },
    { 's', "FIELD", LWMON_OPT_STRING, NULL, { .s = &ts_field },
	   "Name of field containing check timestamps (default: timestamp)" },
    { 'H', "FIELD", LWMON_OPT_STRING, NULL, { .s = &host_field },
	   "Name of field containing the hostname (by default, omitted)" },
    { 'n', "FIELD", LWMON_OPT_STRING, NULL, { .s = &name_field },
	   "Name of field containing item names (default: name)" },
    { 'p', "FIELD", LWMON_OPT_STRING, NULL, { .s = &parm_field },
	   "Name of field containing item parameter names (default: parm)" },
    { 'k', "FIELD", LWMON_OPT_STRING, NULL, { .s = &key_field },
	   "Name of field containing keys (default: key)" },
    { 'd', "FIELD", LWMON_OPT_STRING, NULL, { .s = &value_field },
	   "Name of field containing data values (default: value)" },
    { 'e', "FIELD=VALUE", LWMON_OPT_FUNC, parse_opt_e, { .v = NULL },
	   "Extra data to be added in each INSERT; can be repeated" },

    {  0,  "Help options", LWMON_OPT_TITLE, },
    { 'h', NULL, LWMON_OPT_NONE, NULL, { .i = &show_help },
	   "Show this help text" },
    { 'v', NULL, LWMON_OPT_NONE, NULL, { .i = &show_version },
	   "Show program version" },

    { 0, NULL, LWMON_OPT_END, }
};

static lwmon_opts_t *optlist[] = {
    options,
    NULL
};

static lwmon_opthelp_t *helplist[] = {
    NULL
};

static int parse_opt_e(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _ptr)
{
    const char * equal = strchr(arg, '=');
    if (! equal) {
	fprintf(stderr, "Invalid argument for -%c: '%s' (not in FIELD=VALUE format)\n",
		opt, arg);
	return -1;
    }
    if (num_extras >= MAX_EXTRAS) {
	fprintf(stderr, "Too many -%c options: maximum is %d\n", opt, MAX_EXTRAS);
	return -1;
    }
    extra_keys[num_extras] = arg;
    extra_keylen[num_extras] = equal - arg;
    extra_values[num_extras] = equal + 1;
    num_extras++;
    return 0;
}

static void send_value_n(FILE * F, const char * value, int len) {
    int i;
    putc('\'', F);
    for (i = 0; i < len; i++) {
	putc(value[i], F);
	if (value[i] == '\'') putc(value[i], F);
    }
    putc('\'', F);
}

static void send_value(FILE * F, const char * value) {
    send_value_n(F, value, strlen(value));
}

static int prn_value(const lwmon_packetid_t * I, const lwmon_dataitem_t * D,
		     int is_log, void * F)
{
    int n;
    for (n = 0; n < D->n_values; n++) {
	int xf;
	fprintf(F, "INSERT INTO %s (%s", table, ts_field);
	if (host_field) fprintf(F, ", %s", host_field);
	fprintf(F, ", %s, %s, %s, %s", name_field, parm_field, key_field, value_field);
	for (xf = 0; xf < num_extras; xf++)
	    fprintf(F, ", %.*s", extra_keylen[xf], extra_keys[xf]);
	fprintf(F, ") VALUES (%ld, ", (long)I->timestamp);
	if (host_field) {
	    send_value_n(F, I->host, I->hostlen);
	    fprintf(F, ", ");
	}
	send_value_n(F, D->name, D->namelen);
	fprintf(F, ", ");
	send_value_n(F, D->parm, D->parmlen);
	fprintf(F, ", ");
	if (D->type) {
	    int vp = D->type->max_values;
	    if (vp < 0) vp = -1 - vp;
	    if (vp > n) vp = n;
	    send_value(F, D->type->values[vp].name);
	} else if (is_log) {
	    fprintf(F, "'log'");
	} else {
	    fprintf(F, "'k%d'", n);
	}
	fprintf(F, ", %lld", (long long)D->values[n]);
	for (xf = 0; xf < num_extras; xf++) {
	    fprintf(F, ", ");
	    send_value(F, extra_values[xf]);
	}
	fprintf(F, ");\n");
    }
    return 1;
}

int main(int argc, char *argv[]) {
    lwmon_packet_t P;
    const char * readfile;
    FILE * F;
    lwmon_initops(argc, argv);
    if (! lwmon_opts(optlist) || (! show_help && ! show_version && lwmon_argcount() == 0)) {
	lwmon_shortusage(stderr, "FILE [FILE]...");
	return 1;
    }
    if (show_version || show_help) {
	if (show_version)
	    printf("lwmon-to-sql 0.0 2021-12-19\n");
	if (show_version && show_help)
	    printf("\n");
	if (show_help)
	    lwmon_usage(stdout, "FILE [FILE]...", optlist, helplist);
	return 0;
    }
    if (outfile) {
	F = fopen(outfile, "w");
	if (! F) {
	    perror(outfile);
	    return 2;
	}
    } else {
	F = stdout;
    }
    readfile = lwmon_firstarg();
    while (readfile) {
	int fd = open(readfile, O_RDONLY);
	if (fd < 0) {
	    perror(readfile);
	    return 3;
	}
	while (lwmon_readpacket(fd, &P) > 0)
	    lwmon_packetdata(&P, F, prn_value);
	close(fd);
	readfile = lwmon_nextarg();
    }
    if (outfile) fclose(F);
    return 0;
}

