/* SPDX-License-Identifier: MIT
 *
 * send an lwmon "log" packet to a socket, default /run/lwmon.socket
 * or /tmp/lwmon.socket, depending on which one exist
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <errno.h>
#include <lwmon/options.h>
#include <lwmon/packet.h>

static int show_help = 0, show_version = 0, cmd_measure = 0, cmd_reopen = 0;
static int cmd_terminate = 0, send_fd = -1;
static const char * hostname = NULL;

static int parse_opt_s(char, const char *, int, char *[], lwmon_optdata_t);

static lwmon_opts_t options[] = {
    {  0,  "Socket options", LWMON_OPT_TITLE, },
    { 's', "HOST:PORT|PATH", LWMON_OPT_FUNC, parse_opt_s, { .v = NULL },
	   "Send packed data to UDP HOST:PORT or local socket PATH" },

    {  0,  "Command options", LWMON_OPT_TITLE, },
    { 'm', NULL, LWMON_OPT_NONE, NULL, { .i = &cmd_measure },
	   "Send \"reschedule measures\" command" },
    { 'r', NULL, LWMON_OPT_NONE, NULL, { .i = &cmd_reopen },
	   "Send \"reopen outputs\" command" },
    { 't', NULL, LWMON_OPT_NONE, NULL, { .i = &cmd_terminate },
	   "Send \"terminate\" command" },

    {  0,  "Misc options", LWMON_OPT_TITLE, },
    { 'n', "HOSTNAME", LWMON_OPT_STRING, NULL, { .s = &hostname },
	   "Set host name" },

    {  0,  "Help options", LWMON_OPT_TITLE, },
    { 'h', NULL, LWMON_OPT_NONE, NULL, { .i = &show_help },
	   "Show this help text" },
    { 'v', NULL, LWMON_OPT_NONE, NULL, { .i = &show_version },
	   "Show program version" },

    { 0, NULL, LWMON_OPT_END, }
};

static lwmon_opts_t *optlist[] = {
    options,
    NULL
};

static lwmon_opthelp_t *helplist[] = {
    NULL
};

static const char * socket_names[] = {
    "/run/lwmon.socket",
    "/tmp/lwmon.socket",
    "/tmp/.lwmon.socket",
    NULL
};

static int store_socket(const char * optname, const char * arg) {
    if (send_fd >= 0) {
	close(send_fd);
	send_fd = -1;
    }
    if (arg[0] == '/') {
	struct sockaddr_un in_socket = {
	    .sun_family = AF_UNIX,
	    .sun_path   = "",
	};
	if (strlen(arg) >= sizeof(in_socket.sun_path)) {
	    fprintf(stderr, "Output socket name too long: %s\n", arg);
	    return 0;
	}
	strcpy(in_socket.sun_path, arg);
	send_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (send_fd < 0) goto error1;
	if (connect(send_fd, (struct sockaddr *)&in_socket, sizeof(in_socket)) < 0)
	    goto error1;
	return 1;
    error1:
	perror(arg);
	return 0;
    } else {
	const char * port = strchr(arg, ':');
	if (port) {
	    struct addrinfo hints, * res, * run;
	    int code, hostlen = port - arg;
	    char host[1 + hostlen];
	    strncpy(host, arg, hostlen);
	    host[hostlen] = 0;
	    memset(&hints, 0, sizeof(hints));
#ifdef AI_V4MAPPED
	    hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
#else
	    hints.ai_flags = AI_ADDRCONFIG;
#endif
	    hints.ai_family = AF_UNSPEC;
	    hints.ai_socktype = SOCK_DGRAM;
	    hints.ai_protocol = IPPROTO_UDP;
	    code = getaddrinfo(host, port, &hints, &res);
	    if (code) {
		fprintf(stderr, "%s:%s: %s\n", host, port, gai_strerror(code));
		return 0;
	    }
	    for (run = res; run; run = run->ai_next) {
		send_fd = socket(run->ai_family, run->ai_socktype, run->ai_protocol);
		if (send_fd < 0) goto error2;
		if (connect(send_fd, run->ai_addr, run->ai_addrlen) < 0) goto error2;
		break;
	    error2:
		fprintf(stderr, "%s:%s: %s\n", host, port, strerror(errno));
		if (send_fd >= 0) close(send_fd);
		freeaddrinfo(res);
		return 0;
	    }
	    freeaddrinfo(res);
	    return 1;
	} else {
	    fprintf(stderr,
		    "Invalid argument to %s, need HOST:PORT or PATH\n",
		    optname);
	    return 0;
	}
    }
}

static int parse_opt_s(char opt, const char * arg, int argc, char *argv[],
		       lwmon_optdata_t _od)
{
    const char optname[3] = { '-', opt, 0 };
    return store_socket(optname, arg);
}

int main(int argc, char *argv[]) {
    lwmon_packet_t P;
    const char * text1 = NULL, * text2 = NULL;
    int ok = 1;
    lwmon_initops(argc, argv);
    if (! lwmon_opts(optlist) || (! show_help && ! show_version && lwmon_argcount() > 2)) {
	lwmon_shortusage(stderr, "[TEXT1 [TEXT2]]");
	return 1;
    }
    if (show_version || show_help) {
	if (show_version)
	    printf("lwmon-log 0.0 2022-01-08\n");
	if (show_version && show_help)
	    printf("\n");
	if (show_help)
	    lwmon_usage(stdout, "[TEXT1 [TEXT2]]", optlist, helplist);
	return 0;
    }
    if (send_fd < 0) {
	int i;
	for (i = 0; socket_names[i]; i++) {
	    struct stat sbuff;
	    if (stat(socket_names[i], &sbuff) < 0) continue;
	    if (! S_ISSOCK(sbuff.st_mode)) continue;
	    if (! store_socket(socket_names[i], socket_names[i]))
		return 1;
	    break;
	}
	if (send_fd < 0) {
	    fprintf(stderr, "Cannot find a sending socket, and none specified\n");
	    return 1;
	}
    }
    text1 = lwmon_firstarg();
    if (text1) text2 = lwmon_nextarg();
    if (hostname) lwmon_packet_hostname(hostname);
    lwmon_initpacket(&P);
    if (text1 || text2) {
	ok = lwmon_add_log(&P, text1, text2);
	if (ok <= 0) {
	    fprintf(stderr, "Log text too long\n");
	    return 1;
	}
    }
    /* now try to add command(s) to the packet, if required: if this fails,
     * we'll send 2 packets */
    if (cmd_measure) ok = lwmon_add_command(&P, lwmon_command_measure);
    if (cmd_reopen) ok = lwmon_add_command(&P, lwmon_command_reopen);
    if (cmd_terminate) ok = lwmon_add_command(&P, lwmon_command_terminate);
    if (ok == 0) {
	lwmon_closepacket(&P);
	lwmon_sendpacket(send_fd, &P);
	lwmon_reinitpacket(&P);
	/* adding commands to an empty packet, or to a packet which already
	 * contains other commands, always succeeds so we don't bother
	 * checking for errors again */
	if (cmd_measure) lwmon_add_command(&P, lwmon_command_measure);
	if (cmd_reopen) lwmon_add_command(&P, lwmon_command_reopen);
	if (cmd_terminate) ok = lwmon_add_command(&P, lwmon_command_terminate);
    }
    lwmon_closepacket(&P);
    lwmon_sendpacket(send_fd, &P);
    close(send_fd);
    return 0;
}

