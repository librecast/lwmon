/* SPDX-License-Identifier: MIT
 *
 * system-dependent functions to retrieve monitoring data
 *
 * this file is part of LWMON
 *
 * Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __LWMON_OS_H__
#define __LWMON_OS_H__ 1

#include <lwmon/packet.h>
#include <lwmon/options.h>

/* all functions return nonnegative if OK, negative on error;
 * functions with a callback will return the last value returned
 * by the function, with a negative result stopping processing */

/* determine system load and (if known) number of processors */
typedef struct {
    float loads[3]; /* 1, 5 and 15 minutes normally */
    int processors;  /* 0 if unknown */
} lwmon_os_load_t;

int lwmon_os_load(lwmon_os_load_t *);

/* determine memory/swap used, available, total */
typedef struct {
    long page_size;
    long mem_total;
    long mem_used;
    long mem_free;
    long swap_total;
    long swap_used;
    long swap_free;
} lwmon_os_memory_t;
int lwmon_os_memory(lwmon_os_memory_t *);

/* determine CPU time used since some time in the past, in milliseconds */
typedef struct {
    int64_t user;
    int64_t system;
} lwmon_os_cpu_t;
int lwmon_os_cpu(lwmon_os_cpu_t *);

/* call f(pid, processname, arg) for each running process */
int lwmon_os_process_list(int (*f)(pid_t, const char *, void *), void * arg);

/* call f(fstype, device_path, mount_point, arg) for each mount */
int lwmon_os_disk_list(int (*f)(const char *, const char *,
				const char *, void *),
		       void * arg);

/* call f(proto, port, arg) for all listening sockets matching the first
 * argument */
int lwmon_os_listen_list(lwmon_protocol_t,
			 int (*f)(lwmon_protocol_t, int, void *),
			 void * arg);

/* call f(name, bytes_sent, bytes_recv, arg) for all network interface */
int lwmon_os_interface_list(int (*f)(const char *, int64_t, int64_t, void *),
			    void * arg);

#endif /* __LWMON_OS_H__ */
