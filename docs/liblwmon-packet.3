.TH LIBLWMON-PACKET 3 "21 May 2019" "MKSWAP" "LIBRARIES"
.SH NAME
liblwmon\-packet \- Library to help interfacing to lwmon
.SH SYNOPSIS
.EX
#include <lwmon/packet.h>

void lwmon_packet_hostname(const char *);
void lwmon_initpacket(lwmon_packet_t *);
void lwmon_reinitpacket(lwmon_packet_t *);
int lwmon_receivepacket(int, lwmon_packet_t *, struct sockaddr *, socklen_t *);
int lwmon_add_data(lwmon_packet_t *, const lwmon_dataitem_t *);
const lwmon_datatype_t * lwmon_datatype(const char *);
int lwmon_get_data(const lwmon_packet_t *, int, lwmon_dataitem_t *);
void lwmon_closepacket(lwmon_packet_t *);
void lwmon_printpacket(FILE *, const lwmon_packet_t *, int);
int lwmon_sendpacket(int, const lwmon_packet_t *);
void lwmon_packetid(const lwmon_packet_t *, lwmon_packetid_t *);
.EE
.LP
Link with \fI\-llwmon\fP
.SH DESCRIPTION
The
.I lwmon
library contains functions to prepare and send data to a running
.I lwmon
process, or to receive and decode data from it; this can be used to set
up monitoring not provided natively by
.I lwmon
or to process the data; for example, the included
.I awooga
reporting program uses this library to receive data.
.LP
.SH "SENDING DATA TO LWMON"
To send data to
.I lwmon
or another program compatible with it (such as \fIawooga\fP), one must
first prepare a \fIpacket\fP containing the data, then send it. The
following sequence sets up an empty packet:

.EX
lwmon_packet_t packet;
lwmon_initpacket(&packet);
.EE

The packet will include the local host name as data source; this can be
changed by calling
.BR lwmon_packet_hostname()
before calling
.BR lwmon_initpacket().

Once the packet has been set up, add one or more data items using
.BR lwmon_add_data()
which takes a \fIlwmon_dataitem_t\fP argument, defined as:

.EX
typedef struct {
    const char * name; /* not 0-terminated, see namelen */
    int namelen;
    const char * parm; /* not 0-terminated, see parmlen */
    int parmlen;
    const lwmon_datatype_t * type; /* NULL if unknown / generic */
    int n_values;
    int64_t values[LWMON_VALUES];
} lwmon_dataitem_t;
.EE

The \fIname\fP and \fInamelen\fP fields determine the name of the data
item as will be seen by the receiver; the \fIparm\fP is an optional
"parameter" to further specify what the data contains, and it can be
specified as NULL if not relevant; if not NULL, the \fIparmlen\fP
field must contain its length; the \fItype\fP field is a pointer to
a description of the data, and it needs to be one of \fIlwmon\fP's
predefined types (see below) or NULL for a generic type; a call to
.BR lwmon_datatype()
can help translating a type name into a suitable value for this field;
finally, the actual data is specified as a list of 64-bit integers
in the \fIvalues\fP field, with the length of the list in \fIn_values\fP.
Fixed-point non integer values can be specified by scaling (the
predefined types provide some example of doing that).

The
.BR lwmon_add_data()
function returns 1 if all went well, 0 if there wasn't enough space in the
packet but the data would fit in another packet, and -1 if the data could
never fit because it is bigger than the maximum allowed packet size; after
a return value of 0, the caller could decide to send the packet on and
produce a "continuation" packet, described below.
.LP
Once all the data has been filled in, a single call to
.BR lwmon_closepacket()
prepares the packet for sending, and any number of calls to either
.BR lwmon_printpacket()
or
.BR lwmon_sendpacket()
will send the data on, with
.BR lwmon_printpacket()
producing a human-readable representation to an already opened file, and
.BR lwmon_sendpacket()
encoding the packet in a compressed binary format and sending it on an
already opened socket; in both cases, the first argument indicates where
to send the data (file or socket), and the second argument is the packet
itself; for
.BR lwmon_printpacket()
there is a third argument indicating the level of detail required (0
means terse, 1 means detailed).
.LP
If any call to
.BR lwmon_add_data()
returned 0, it is possible to construct a "continuation" packet by calling
.BR lwmon_reinitpacket()
and then repeating the call to
.BR lwmon_add_data().
This keeps the same packet serial number and timestamp, so the receiver
knows that they are part of the same transmission (a packet sequence number
will be increased to make sure the receiver does not treat the new packet
as a duplicate).
.LP
.SH "RECEIVING DATA FROM LWMON"
The following sequence waits for a packet on an open socket:

.EX
lwmon_packet_t packet;
struct sockaddr_storage_t sender;
socklen_t sender_len = sizeof(sender);
lwmon_receivepacket(file, &packet, &sender, &sender_len);
.EE

The decoded data will be put in \fIpacket\fP, and if \fIsender\fP is not
NULL it will be filled with the sender's address (which needs to be long
enough to contain an address in the address family specified for the
socket), as well as adjusting \fIsender_len\fP to contain the size of
the address, if it is smaller than the buffer provided.

The
.BR lwmon_receivepacket()
function returns 1 if data arrived correctly, 0 on an end of file condition
(or if the sender has closed the socket), and a negative value for an error,
in which case it will set \fIERRNO\fP appropriately.
.LP
Some information about the packet received by
.BR lwmon_receivepacket()
can be obtained by calling
.BR lwmon_packetid()
on that packet; this will fill a structure defined as:

.EX
typedef struct {
    time_t timestamp;
    const char * host;  /* not 0-terminated, see hostlen */
    int hostlen;
    int id;             /* id number helps detecting duplicates */
    int seq;            /* sequence number within a block of packets */
    int checksum;
} lwmon_packetid_t;
.EE
.LP
The actual data can be decoded from the packet by calling
.BR lwmon_get_data()
passing the packet itself, the number of the data item (0 will be the
data added by the first call to
.BR lwmon_add_data(),
1 will be the data added by the next call, and so on), and a structure
where the data will be stored; this function returns 1 if OK, 2 if
the data item is a log entry, 0 if
the second argument is too large (more than the actual number of data
items in the packet), and -1 if there was a decoding error.
.LP
.SH "DATA TYPES"
TBW
.LP
.SH "SEE ALSO"
.BR awooga (1),
.BR lwmon (1)
.SH BUGS
None that we know of. If we do become aware of a bug, we'll work
to fix it.
.SH COPYRIGHT
Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

