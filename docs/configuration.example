# Example configuration file for lwmon

# sets the hostname sent with packets (default, the local host name)

hostname lwmon-test

# all checks are specified with the generic format:

# CHECK_NAME NAME_SENT FREQUENCY [CHECK_ARGUMENTS]

# where CHECK_NAME is the name as known to the program and explained below;
# NAME_SENT is the name actually sent to the monitoring server(s) or logged
# to files; FREQUENCY is the number of seconds between checks; CHECK_ARGUMENTS
# are any additional arguments required, as explained below

# The data sent or logged will always have the form:

# NAME_SENT [PARAMETER] NUMBERS

# where PARAMETER is a check-specific string, such as a mountpoint or the
# pattern which matched; NUMBERS is a (possibly empty) list of numbers which
# represent the result of the check

# sends binary reports to a server on SERVER_HOST, port 2345, as well as
# another to BACKUP_HOST, port 12345

send SERVER_HOST:2345 BACKUP_HOST:12345

# also send binary reports to local socket

send /run/log-monitoring.socket

# also logs all data to file /var/log/lwmon.log, appending to existing
# file if already present, in printable (human-readable) format

print /var/log/lwmon.log append

# and to /tmp/lwmon.log, overwriting file if present:

print /tmp/lwmon.log overwrite

# (if the program receives a SIGHUP it will close and reopen these files,
# continuing to append to /var/log/lwmon.log unless it has been rotated and
# a new file created; however after a SIGHUP it will overwrite /tmp/lowmon.log)

# Check disk usage on all ext2/3/4 filesystems every 15 seconds,
# sending it as:
# ext+ MOUNTPOINT BLOCK_SIZE TOTAL_BLOCKS FREE_BLOCKS AVAILABLE_BLOCKS TOTAL_INODES FREE_INODES AVAILABLE_INODES

disk ext+ 15 type ext[345]

# and check also anything mounted under /mnt, every minute, sending as
# mnt MOUNTPOINT BLOCK_SIZE TOTAL_BLOCKS FREE_BLOCKS AVAILABLE_BLOCKS TOTAL_INODES FREE_INODES AVAILABLE_INODES

disk mnt 60 mount /mnt/*

# and any mounted LVM volume with volume group volg every 10 seconds:

disk lvm 10 device /dev/mapper/volg-*

# checks can be conditional on local host name, or existence of a
# file when the program starts; for example the following checks
# MMC disks if running on a host with name starting with "phone-"
# or "tablet-"

if host tablet-* phone-*
    disk mmc 60 device /dev/mmcblk*
endif

# the hostname used here is the local host name, not the one set by
# "hostname" or "-l"

# and the following makes checks more frequent if a file exists under
# /var/checks or /etc/checks when the program starts

if file /var/checks/* /etc/checks/*
    disk disks 5 mount /mnt/*
else
    disk disks 60 mount /mnt/*
endif

# Same, but there needs to be a nonempty file:

if nonempty /var/checks/* /etc/checks/*
    disk disks 5 mount /mnt/*
else
    disk disks 60 mount /mnt/*
endif

# it is also possible to send a report if a file exists: this checks
# every minute to see if a file is present under /var/log/errors or
# /var/log/warnings; if no such file is present, sends:
# report "" 0
# if a file is found matching PATTERN (/var/log/errors/* or
# /var/log/warnings/* in this example) it sends:
# report PATTERN 1

file report 60 /var/checks/* /etc/checks/*

# same, but only notices nonempty files:

nonempty report 60 /var/checks/* /etc/checks/*

# Check if there are processes listening on TCP port 80 and 443, sending:
# service NUMBER_PORT_80 NUMBER_PORT_443
# (the number is the number of open listening sockets, rather than the
# number of listening processes, so each number is normally 0 if nobody
# is listening, 1 if at least one process listens to just 1 socket, and 2
# if there are sockets on both the IPv4 and IPv6 address families)

listen service 60 TCP 80 443

# or equivalently

listen service 60 TCP http https

# or to get separate IPv4 and IPv6 counts:

listen service4 60 TCP4 http https
listen service6 60 TCP6 http https

# and also check that a DNS server and an NTP server are running sending:
# udpservice NUMBER_DNS NUMBER_NTP

listen udpservice 60 UDP 53 123

# or maybe

listen udpservice 60 UDP domain ntp

# or even

listen udpservice4 60 UDP4 domain ntp
listen udpservice6 60 UDP6 domain ntp

# Check load average every minute and send
# lavg 1MINUTE 5MINUTES 15MINUTES N_PROCESSORS
# where the load numbers are multiplied by 100 and converted to integers,
# and N_PROCESSORS is the number of processors, if known (if not known,
# the number is not included)

load lavg 60

# Check memory and swap usage every minute and send
# memswap PAGE_SIZE TOTAL_MEM_PAGES USED_MEM_PAGES FREE_MEM_PAGES TOTAL_SWAP_PAGES USED_SWAP_PAGES FREE_SWAP_PAGES

memory memswap 60

# send the status of all local interfaces with names matching eth* or wlan*
# every minute; the report will have the form:
# intf NAME SECONDS_SINCE_LAST_REPORT BYTES_SENT_SINCE_LAST_REPORT BYTES_RECEIVED_SINCE_LAST_REPORT
# repeated for every matching interface (the list of interfaces is determined
# when the program starts)

network intf 60 eth* wlan*

# Send the total number of processes running every 30 seconds as
# tot_proc NUMBER

processes tot_proc 30

# and also send the number of apache2, httpd and commands starting with
# "spec_" as
# num_proc NUMBER_APACHE2 NUMBER_HTTPD NUMBER_SPEC

processes num_proc 30 apache2 httpd spec_*

# run a program once and report the resources it used; after the program
# terminates, the check is removed (the check ends as soon as the program
# terminates; however if for some reason the SIGCHLD does not arrive,
# the monitoring also uses the frequency to periodically determine if the
# program is still running); if this check is used at least once, it changes
# the behaviour of monitoring, so that once all the "removable" checks have
# been removed, all remaining checks start being removed as soon as they run,
# and the monitoring terminates when no more checks are left: in other word,
# using this check will run the monitoring until all programs have terminated
# and all checks have run one more time; the program runs with standard
# input, output and error redirected to /dev/null but of course it can
# reopen any of these files if it needs logging or anything.
# If the fork() fails, it is retried for up to 5 times, at the frequency
# specified; however if the program fails to run it'll be reported as
# done, with the appropriate exit status
#
# The check results are sent as:
# run_done WALL_TIME_MSEC USER_CPU_MSEC SYSTEM_CPU_MSEC EXIT_STATUS

program run_done 5 /usr/local/bin/experiment --data /etc/experiment-data

# Every minute, send a count of logged-on users as:
# login NUMBER

usercount login 60

# Also send a complete list of logged-on users every hour, as:
# user USER1 COUNT1
# user USER2 COUNT2
# user USER3 COUNT3
# ...
# (there is a compiled-in limit to the length of this list); if the
# list is empty (nobody logged in) it sends a single:
# user 0

userlist user 3600

# send statistics about lwmon's own resource usage every minute,
# averaged so it reports usage per second as:
# lwmon USER_CPU_MSEC_PER_SEC SYSTEM_CPU_MSEC_PER_SEC MAX_RSS PAGE_FAULTS_PER_SEC

self lwmon 60

# receive additional monitoring data from a local socket - a program
# using liblwmon can send packets there for forwarding to all defined
# destinations and any other defined processing

receive /run/lwmon.sock

# also listen on UDP port 12345 and localhost:2345

receive 12345
receive localhost:12345

# permit receiving commands from a remote source

# permit "reopen outputs" command if they have been open for more than
# 5 seconds

cmd_reopen 5

# permit "reschedule measurements" command on the commands listed if they
# haven't run less than 100ms ago

cmd_measure 100 ext+ mnt lvm

