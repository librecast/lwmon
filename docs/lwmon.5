.TH LWMON 5 "11 May 2019" "MKSWAP" "FILE FORMATS"
.SH NAME
lwmon.conf \- configuration file for ddb
.SH DESCRIPTION
When the
.B lwmon
program starts, it needs to determine what to monitor and where to
send the data; a simple way to provide this information is by means
of a configuration file (or files); there is no default configuration
file, a file or a directory contaning configuration files needs to
be specified on the command line; this manual page describes the
format of such files.
.LP
Blank lines and lines containing a "#" as the first nonblank character
are ignored (note that a "#" elsewhere in the line does not have any
special meaning); a "\\" at end of line indicates that the line
continues on the next line; each line consists of a sequence of
fields separated by space where the first field is a keyword and
the rest its arguments; if any field contains spaces, it can be
quoted in single or double quotes, or enclosed in brackets.
.LP
.SH "MONITORING SOURCES"
The \fBlwmon\fP program can get monitoring data by accessing local
resources; these are specified in general using the format:

.EX
\fIKEYWORD\fP \fINAME\fP \fIFREQUENCY\fP [\fIARGUMENT\fP...]
.EE

The \fIKEYWORD\fP specifies what is going to be monitored, and is
described in this section; the \fINAME\fP is used to identify the
data being collected (so for example the same item may be monitored
more than once, with different arguments, and the data distinguished
by specifying different \fINAME\fPs).
.LP
The \fIFREQUENCY\fP specifies how often this particular item is
checked, indicated as a number of seconds between checks.
.LP
The \fIARGUMENT\fPs depend on the \fIKEYWORD\fP.
.LP
The folllowing \fIKEYWORD\fPs specify monitoring sources:
.LP
.TP 0.5i
disk
Collects information about disk space free and used; the first
\fIARGUMENT\fP indicates how to select mounted disks (\fImount\fP
to select by mountpoint, \fIdevice\fP by device name normally in
"/dev", and \fItype\fP by filesystem type, for example "ext4");
the remaining \fIARGUMENT\fPs are patterns to match, so for example
the following monitors all disks which are formatted using the
ext2, ext3 or ext4 filesystem:

.EX
disk all_ext4 60 type ext[234]
.EE

If there are no \fIARGUMENT\fPs (or if there is just the first
\fIARGUMENT\fP but no patterns), by default all disks are monitored.
The data sent for each disk will have the format:

.EX
\fINAME\fP \fIMOUNTPOINT\fP \fIBLOCK_SIZE\fP
\fITOTAL_BLOCKS\fP \fIUSED_BLOCKS\fP \fIFREE_BLOKS\fP \fIAVAILABLE_BLOCKS\fP
\fITOTAL_INODES\fP \fIUSED_INODES\fP \fIFREE_INODES\fP \fIAVAILABLE_INODES\fP
.EE

.TP 0.5i
file
Checks for the presence of a file; each \fIARGUMENT\fP is a pattern indicating
what files to look for; if none of the patterns match an existing file, it
returns:

.EX
\fINAME\fP "" 0
.EE

(That is \fINAME\fP followed by an empty string and the number 0); if
any patterns match, it returns the first which matched:

.EX
\fINAME\fP \fIPATTERN\fP 1
.EE

.TP 0.5i
listen
Reports how many sockets are listening on a specified TCP or UDP port;
the first \fIARGUMENT\fP indicates the protocol, which is either "udp"
or "tcp", optionally followed by a "4" or "6" to restrict the check to
IPv4 and IPv6, respectively (there must be no space between the "udp"
or "tcp" and the number);
the remaining \fIARGUMENT\fPs are the port numbers or service names.
The data returned will contain a number for each port \fIARGUMENT\fP,
indicating the number of sockets listening on that port, for example
specifying:

.EX
listen L tcp4 http gopher
.EE

will send:

.EX
L 1 0
.EE

when there is a web server running, but not a gopher server.
.TP 0.5i
load
The load average as returned by the operating system, normally as three
numbers indicating the averages over the last minute, 5 minutes and
15 minutes, respectively; these are multiplied by 100 and converted to
integers, so that a load of 0.42 returns the number 42. Sends:

.EX
\fINAME\fP \fI1_MINUTE_LOAD\fP \fI5_MINUTES_LOAD\fP \fI15_MINUTES_LOAD\fP
.EE

.TP 0.5i
memory
Returns information about memory usage as:

.EX
\fINAME\fP \fIPAGE_SIZE\fP \fITOTAL_MEMORY\fP \fIMEMORY_USED\fP \fIMEMORY_FREE\fP
\fITOTAL_SWAP\fP \fISWAP_USED\fP \fISWAP_FREE\fP
.EE

Apart from the page size, all numbers are in kilobytes.
.TP 0.5i
network
Collects information about amount of data sent and received on network
interfaces; if any \fIARGUMENT\fPs are specified, these will be patterns
matching interface names; if no \fIARGUMENT\fPs are specified, the
program will report on all interfaces; note that the list of interfaces
is determined when the program starts. Returns the following for each
interface:

.EX
\fINAME\fP \fIINTERFACE\fP \fITIME_SINCE_LAST_CHECK\fP
\fIBYTES_SENT_SINCE_LAST_CHECK\fP \fIBYTES_RECEIVED_SINCE_LAST_CHECK\fP
.EE

.TP 0.5i
nonempty
Like "file" described above, but ignores empty files (so that it will return
zero if files are present but empty).
.TP 0.5i
processes
Collects a count of running processes; if no \fIARGUMENT\fPs are specified
it returns a single number, the total number of processes:

.EX
\fINAME\fP \fINUMBER_OF_PROCESSES\fP
.EE

If there are \fIARGUMENT\fPs, they are used to match process names, and
the data return will include a number corresponding to each \fIARGUMENT\fP;
for example:

.EX
processes P httpd apache* lwmon
.EE

will return three numbers, the number of processes whose name is "http",
whose name starts with "apache", and whose name is "lwmon", respectively.
The keyword "processes" can be abbreviated to "procs".
.TP 0.5i
program
Runs a program once, then reports on its resource usage:

.EX
\fINAME\fP \fIELAPSED_TIME\fP \fIUTIME\fP \fISTIME\fP \fIEXIT_STATUS\fP
.EE

where the various "\fI_TIME\fP" values are expressed in milliseconds
and the \fIEXIT_STATUS\fP is what is returned by \fBwaitpid(2)\fP
so it contain a signal number or a return code.

If this check is specified at least once, \fBlwmon\fP will automatically
stop after all programs started by it have terminated, and all remaining
checks have ran at least once after the programs terminated.
.TP 0.5i
self
Monitors resource usage by the \fBlwmon\fP program itself; sends:

.EX
\fINAME\fP \fIAVG_UTIME\fP \fIAVG_STIME\fP \fIMAXRSS\fP \fIAVG_FAULTS\fP
.EE

where the "\fIAVG_\fP" values are averages per second of user-mode CPU
time, system-mode CPU time and page faults, respectively; the \fIMAXRSS\fP
number indicates the maximum amount of memory used since startup.
.TP 0.5i
usercount
Monitors how many users are logged on, returning the total number as:

.EX
\fINAME\fP \fINUMBER_OF_USERS\fP
The keyword "usercount" can be abbreviated to "users".
.TP 0.5i
userlist
Monitors which users are logged on, and returns a list of such users,
by sending a data item for each distinct user:

.EX
\fINAME\fP \fIUSERNAME\fP \fICOUNT\fP
.EE

where \fICOUNT\fP is the number of times the \fIUSERNAME\fP is logged in.
If there is nobody logged in, it sends a single "empty string" result with
a count of 0:

.EX
\fINAME\fP "" 0
.EE

.PP
.LP
.SH CONDITIONALS
To simplify producing a configuration file usable on different machines,
it is possible to include or exclude sections based on conditions.
Each conditional section is introduced by:

.EX
if \fIITEM\fP \fIPATTERN\fP...
.EE

where \fIITEM\fP is one of the items described below, and the section
will be included if the \fIITEM\fP matches one of the \fIPATTERN\fPs,
omitted oterwise.
.LP
.TP 0.5i
has_file
True if there exists a file matching one of the \fIPATTERN\fPs at the time
the program starts up; this can be abbreviated to "file".
.TP 0.5i
nonempty_file
Same as "has_file", but empty files are ignored; this can be abbreviated
to "nonempty".
.TP 0.5i
host_is
True if the local host name matches one of the \fIPATTERN\fPs; this can
be abbreviated to "host".
.PP
.LP
A conditional can be followed by any number of sections introduced by:

.EX
elsif \fIITEM\fP \fIPATTERN\fP...
.EE

which will be included only if the condition is true, and none of the
previous conditions (in the same conditional) was true. The conditional
can be optionally followed by a single section introduced by:

.EX
else
.EE

with the usual meaning that the section will be included only if none
of the previos ones were. The conditional terminates with:

.EX
endif
.EE
.LP
.SH "RECEIVING DATA FROM ELSEWHERE"
In addition to the data it collects, the program can also receive data
from other sources; these are specified with:

.EX
receive [\fIADDRESS\fP:]\fIPORT\fP
receive \fI/path/to/socket\fP
.EE

Both forms receive compressed binary representation of the data;
the first form receives UDP packets on the specified \fIPORT\fP,
and if specified, only on interfaces matching \fIADDRESS\fP;
the second form creates a local socket at the path specified and
receives packets on it.
.LP
Another copy of \fBlwmon\fP can send data using the "receive" keyword,
described in the next section; a different program can also send
data if it encodes it in the correct format, probably using functions
from \fBliblwmon\fP.
.LP
.SH "SENDING AND LOGGING DATA"
The program can send data to another copy of \fBlwmon\fP or to any
other program which can receive and use the information; this is
done with:

.EX
send \fIHOST\fP:\fIPORT\fP
send \fI/path/to/socket\fP
.EE

The first form sends UDP packets to the destination specified, while
the second form sends to a local socket, which must already exist
when the program starts, and must already have something listening
to it.
.LP
Data can also be logged to a file or standard output using:

.EX
print \fI/path/to/file\fP [\fIAPPEND\fP [\fIFORMAT\fP]]
.EE

The optional second argument is either "append" to add the data to an
existing file (a new file will still be created if it does not exist),
or "overwrite" to replace the contents if the file already exists;
if the argument is not specified, the default is "overwrite".

The optional third argument determines the output format;
if omitted, or specified as "terse", just the data
will be included; if specified as "verbose" or "details", the program
will included a description of the data, if known; if specified
as "binary", the data will have the same format as the UDP packets
produced by "send".

If the program receives a "hang up" signal (SIGHUP) it will close and
reopen all log files (note that the reopening always assumes that
"append" was specified, only the first open can overwrite data).
.LP
All information sent or logged includes the hostname where it was
generated: data generated locally includes the local host name, and
data received from other sources specifies the hostname where it was
generated; to send a different hostname with locally-generated data, use:

.EX
hostname \fINAME\fP
.EE
.LP
.SH "EXTERNAL COMMANDS"
If the program is receiving data from sockets, it can be made to
execute commands received with that data. The
.BR lwmon-log (1)
program can send these commands.  The commands will be ignored unless
the configuration file asks to execute them.  The "reopen outputs"
command is equivalent to receiving a "hang up" signal and is enabled
with:

.EX
cmd_reopen \fISECONDS\fP
.EE

if the command is received, and at least the specified number of
seconds have passed since the logs were last reopened (or since
startup, if no reopen has yet happened), the command will be
executed.  If this option appears more than once, the last one
defines the frequency and previous ones are ignored.

The "reschedule measure" command asks to re-run measurements
"immediately" if permitted.  The option:

.EX
cmd_measure \fIMILLISECONDS\fP \fINAME\fP [\fINAME\fP]...
.EE

allows the check named to be rescheduled if they have ran at least
the specified number of milliseconds ago; if multiple options
specify the same checks, the one with the lowest number of
milliseconds wins.

The "terminate" command causes the program to close all outputs
and terminate; this is only accepted if the configuration contains:

.EX
cmd_terminate
.EE
.LP
.SH "SEE ALSO"
.BR lwmon (1),
.BR lwmon-log (1),
.BR liblwmon (3)
