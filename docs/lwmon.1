.TH LWMON 1 "29 April 2019" "MKSWAP" "LOCAL USER COMMANDS"
.SH NAME
lwmon \- light weight system monitor
.SH SYNOPSIS
.EX
\fBlwmon\fP [options]...
.EE
.SH DESCRIPTION
The purpose of the
.B lwmon
utility is to collect system monitoring data while using as little
resources as possible, to increase the likelyhood that the program
will continue functioning when the system is running out of resources,
and therefore will be able to report the situation.  This appears
to be in contrast with most other monitoring systems which may use
more resources than the thing they are supposed to monitor.
.LP
The normal way to invoke
.B lwmon
is to provide a confuration file (see
.BR lwmon (5)
for information about configuration files) and use the syntax:
.EX

\fBlwmon\fP \fB\-c\fP \fICONFIGURATION_FILE\fP \fB\-f\fP \fB\-i\fP \fIPID_FILE\fP

.EE
which starts lwmon in the background writing its process ID to
.I PID_FILE
and uses the
.I CONFIGURATION FILE
specified to determine what to monitor, and where to send the information.
Other options are described below.
.LP
.SH OPTIONS
.TP 0.5i
.B \-a \fIFILE\fP
Appends a human-readable summary of the data collected to the file
specified; this can be useful for debugging. This option is equivalent
to specifying the \fBprint\fP option in a configuration file, and
can be repeated to log information to more than one file. See also
\fB\-A\fP, \fB\-p\fP and \fB\-P\fP. If the program receives a \fISIGHUP\fP it
will close and reopen all such files.
.TP 0.5i
.B \-A \fIFILE\fP
Like \fB\-a\fP, but includes more details about the data collected.
.TP 0.5i
.B \-c \fICONFIGURATION_FILE\fP
Read configuration from the file specified; this option can be
repeated (and mixed with \fB\-d\fP and \fB\-o\fP described below)
in which case the options are applied in the order the files
appear in the command line.
.TP 0.5i
.B \-d \fIDIRECTORY\fP
Reads configuration from all files in the directory specified (excluding
files whose name starts with a period), processing the files in
lexycographic order (assuming "C" locale, for consistent processing
independent on the environment).
.TP 0.5i
.B \-f
Run in the background by forking after reading all configuration; option
\fB\-i\fP can be used to find out the process ID of the program.
.TP 0.5i
.B \-h
Show usage message and exit.
.TP 0.5i
.B \-i \fIPID_FILE\fP
Writes the process ID in the file specified.
.TP 0.5i
.B \-n \fIHOSTNAME\fP
Specifies the hostname sent with the data collected; the default is to
use the system's hostname.
.TP 0.5i
\fB\-o\fP '\fIOPTION\fP \fIVALUE\fP [\fIVALUE\fP...]'
Specifies a single configuration option, as though it had been read from
a single-line file using \fB\-c\fP. Note that the configuration file
syntax requires a space between the option name and its value(s), therefore
the whole string needs to be quoted.
.TP 0.5i
.B \-p \fIFILE\fP
Like \fB\-a\fP, but the file is overwritten if it already exist. If \fIFILE\fP
is "-", the data will be printed to the program's standard output (this is
not possible if the program runs in the background).
.TP 0.5i
.B \-P \fIFILE\fP
Like \fB\-A\fP, but the file is overwritten if it already exist, and like \fI\-p\fP
the \fIFILE\fP can be "-" to indicate standard output.
.TP 0.5i
.B \-R \fIFILE\fP
Reads binary data from \fIFILE\fP and sends it to all defined outputs;
no other processing will be performed.
.TP 0.5i
\fB\-r\fP [\fIHOST\fP\fB:\fP]\fIPORT\fP
Listens for UDP packets on the specified port (and if specified, arriving
on the interfaces corresponding to \fIHOST\fP); such packets will be forwarded
to all destinations specified just like packets originated locally by the
program.
This is equivalent to specifying \fBreceive\fP in
a configuration file. There is a compiled-in limit in the number of listening
sockets the program will open.

For example this could run on a firewall to forward monitoring data
from outside to inside:
.EX

\fBlwmon\fP -r 12345 -s OTHER_HOST:12345

.EE
For another example, the following command would receive
and print the data sent by another copy of \fBlwmon\fP using \fB\-s\fP:
.EX

\fBlwmon\fP -r 12345 -p -
.EE
.TP 0.5i
.B \-r \fIPATH\fP
Listens on a UNIX-domain socket located at the path specified (which
must start with a "/"); the data accepted will have the same format as
the data received via UDP; this could be useful to have other programs
sending monitoring data to be processed.

For example, a program could run from
.BR cron(1)
to send information which is not collected directly by \fBlwmon\fP, and
this program could send the information to a UNIX-domain socket, perhaps
using
.BR liblwmon(3)
to prepare and send the data.
.TP 0.5i
.B \-s \fIHOST\fP:\fIPORT\fP
Sends UDP packets with the data collected to the sprecified host and port;
the data is sent in a compressed binary format, see option \fI\-r\fP for
how to receive this data. This is equivalent to specifying \fBsend\fP in
a configuration file.
There is a compiled-in limit in the number of destinations specified with \fB\-s\fP.
.TP 0.5i
.B \-s \fIPATH\fP
Sends packets with the data collected to a UNIX-domain socket located at
the path specified (which must start with a "/"); this uses the same
binary format as when sending via UDP, and shares the same compiled-in
limit on the number of destinations.
.PP
.TP 0.5i
.B \-v
Show program version and exit.
.LP
.SH "SEE ALSO"
.BR liblwmon (3),
.BR lwmon (5)
.SH BUGS
None that we know of. If we do become aware of a bug, we'll work
to fix it.
.SH COPYRIGHT
Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

