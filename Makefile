# SPDX-License-Identifier: MIT
#
# this file is part of LWMON
#
# Copyright (c) 2010-2022 Claudio Calvelli <clc@librecast.net>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

PREFIX = /usr/local

ABIVERSION = 0.1
LIBVERSION = 0.1.0

CFLAGS += -Iinclude -O -Wall -Iobj/include
ifeq ($(MAKE_STATIC),)
CFLAGS_LIB = -fPIC
CFLAGS_MODULE = -fPIC
LIBNAME = liblwmon.so.$(LIBVERSION)
LDFLAGS_LIB = -shared -fPIC -Wl,-soname,$(LIBNAME)
LDFLAGS_BIN = -Lobj/build/objects -llwmon
LDFLAGS_BIN2 = -llwmon
LIBRARY = obj/build/objects/liblwmon.so
BIN_DEPS = $(LIBRARY)
INST_DEPS = install_lib
else
CFLAGS_LIB = 
CFLAGS_MODULE = 
LDFLAGS_LIB = 
LDFLAGS_BIN = $(OBJECTS_BUILD)
LDFLAGS_BIN2 =
LIBRARY = 
BIN_DEPS = $(OBJECTS_BUILD)
INST_DEPS =
endif

ifeq ($(PREFIX),/)
INSTALL_BIN = /usr/bin
INSTALL_LIB = /usr/lib
INSTALL_MAN = /usr/share/man
INSTALL_INCLUDE = /usr/include
else
INSTALL_BIN = $(PREFIX)/bin
INSTALL_LIB = $(PREFIX)/lib
INSTALL_MAN = $(PREFIX)/share/man
INSTALL_INCLUDE = $(PREFIX)/include
endif

ifeq ($(DESTDIR),)
LDCONFIG ?= ldconfig
else
LDCONFIG ?= ldconfig -n
endif

OSNAME = $(shell uname -s)

PROGRAMS = lwmon lwmon-log lwmon-to-sql
OBJECTS = packet options os-$(OSNAME)
INCLUDES = include/lwmon/packet.h include/lwmon/options.h
MANPAGES = docs/*.[135]

OBJECTS_BUILD = $(shell for name in $(OBJECTS); do echo "obj/build/objects/$$name.o"; done)
PROGRAMS_BUILD = $(shell for name in $(PROGRAMS); do echo "obj/bin/$$name"; done)
PROGRAMS_INT = $(shell for name in $(PROGRAMS); do echo "obj/build/bin/$$name.o"; done)

OBJDIR = obj obj/build obj/build/bin obj/build/objects obj/bin obj/include obj/include/lwmon
all : build
build : $(OBJDIR) obj/include/lwmon/configure.h $(OBJECTS_BUILD) $(LIBRARY) $(PROGRAMS_INT) $(PROGRAMS_BUILD)

install : install_bin install_includes $(INST_DEPS) install_man

.PHONY: clean
clean: 
	rm -rf obj/build

.PHONY: realclean
realclean: 
	rm -rf obj

install_bin : all
	install -d $(DESTDIR)$(INSTALL_BIN)
	install $(PROGRAMS_BUILD) $(DESTDIR)$(INSTALL_BIN)

install_lib : all
	install -d $(DESTDIR)$(INSTALL_LIB)
	install obj/build/objects/liblwmon.so.$(LIBVERSION) $(DESTDIR)$(INSTALL_LIB)
	ln -sf $(LIBNAME) $(DESTDIR)$(INSTALL_LIB)/liblwmon.so
	$(LDCONFIG) $(DESTDIR)$(INSTALL_LIB)

install_includes : all
	install -d $(DESTDIR)$(INSTALL_INCLUDE)
	install -d $(DESTDIR)$(INSTALL_INCLUDE)/lwmon
	install include/lwmon.h $(DESTDIR)$(INSTALL_INCLUDE)
	install $(INCLUDES) $(DESTDIR)$(INSTALL_INCLUDE)/lwmon

install_man : all
	install -d $(DESTDIR)$(INSTALL_MAN)
	install $(MANPAGES) $(DESTDIR)$(INSTALL_MAN)

$(OBJDIR) :
	mkdir $@

obj/include/lwmon/configure.h :
	echo '/* automatically generated, edit at your own risk */' > $@.tmp
	if [ -f /usr/include/endian.h ]; then echo '#define HAS_ENDIAN_H' >> $@.tmp; fi
	mv $@.tmp $@

obj/build/objects/liblwmon.so: obj/build/objects/liblwmon.so.$(LIBVERSION)
	ln -sf liblwmon.so.$(LIBVERSION) $@

obj/build/objects/liblwmon.so.$(LIBVERSION): $(OBJECTS_BUILD)
	$(CC) $(LDFLAGS) $(LDFLAGS_LIB) -o $@ $(OBJECTS_BUILD)

obj/bin/%: obj/build/bin/%.o $(BIN_DEPS)
	$(CC) $(LDFLAGS) $(LDFLAGS_BIN) -o $@ $< $(LDFLAGS_BIN2)

obj/build/bin/%.o: src/%.c $(INCLUDES) Makefile
	$(CC) $(CFLAGS) -o $@ -c $<

obj/build/objects/%.o: src/%.c $(INCLUDES)
	$(CC) $(CFLAGS) $(CFLAGS_LIB) -o $@ -c $<

